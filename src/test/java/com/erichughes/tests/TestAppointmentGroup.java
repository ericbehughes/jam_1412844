/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erichughes.tests;


import com.ericbehughes.database.JamDbUtil;
import com.ericbehughes.database.JamDAO;
import com.ericbehughes.models.AppointmentGroupRecord;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ehugh
 */
public class TestAppointmentGroup {

    private final Logger log = LoggerFactory.getLogger(
            this.getClass().getName());
    private final JamDbUtil dbUtil = new JamDbUtil();
    private final JamDAO jamObj = dbUtil.getJamDAO();

    

    /**
     * This will insert a record, retrieve the just inserted record and compare the inserted and retrieved objects
     *
     * @throws SQLException
     */
    @Test
    public void testAppointmentRecordGroupCreate() throws SQLException {
        log.debug("start testAppointmentRecordGroupCreate()");
        AppointmentGroupRecord group1 = new AppointmentGroupRecord();
        group1.setGroupName("SomeGroupName");
        group1.setColor("000000");
        int result = jamObj.create(group1);
        log.debug(">>>>>>>>>>>>>>>>>>>>>" + group1.getGroupName());

        AppointmentGroupRecord group2 = jamObj.findAppointmentGroupByName(group1.getGroupName());
        log.debug(">>>>>>>>>>>>>>>>> group1  \n\n" + group1.toString());
        log.debug(">>>>>>>>>>>>>>>>> group2  " + group2.toString());
        assertEquals("A record was not created", group1, group2);
        log.debug("end testAppointmentRecordGroupCreate()");
    }

    /**
     * This will insert a record, retrieve the just inserted record and compare the inserted and retrieved objects
     *
     * @throws SQLException
     */
    @Test (expected = SQLException.class)
    public void testCreateAppointmentRecordGroupFailure() throws SQLException {
        log.debug("start testCreateAppointmentRecordGroupFailure() ");
        AppointmentGroupRecord group1 = new AppointmentGroupRecord();
        group1.setGroupName("SomeGroupName");
        group1.setColor("000000765r43w");
        int result = jamObj.create(group1);
        log.debug(">>>>>>>>>>>>>>>>>>>>>" + group1.getGroupName());
        AppointmentGroupRecord group2 = jamObj.findAppointmentGroupByName(group1.getGroupName());
        log.debug(">>>>>>>>>>>>>>>>> group1  \n\n" + group1.toString());
        log.debug(">>>>>>>>>>>>>>>>> group2  " + group2.toString());
        //Assert.assertFalse(!group1.equals(group2));
        fail("either way its a fail");
        log.debug("end testCreateAppointmentRecordGroupFailure() ");
    }
    /**
     * Test of testDeleteAppointmentGroup method, of class JamDAO.
     * 
     * @throws SQLException 
     */
    @Test
    public void testfindAllGroups() throws SQLException {
        log.debug("start testfindAllGroups()");
        List<AppointmentGroupRecord> list = jamObj.findAllGroups();
        log.debug(">>>>>>>>>>>>>>>>> Find all groups  " + list.toString());
        assertEquals(7, list.size());
        log.debug("end testfindAllGroups()");
    }


    /**
     * Test of testDeleteAppointmentGroup method, of class JamDAO.
     *
     * @throws java.sql.SQLException
     */
    @Test
    public void testDeleteAppointmentGroup() throws SQLException {
        log.debug("start testDeleteAppointmentGroup()");
        int testId = 4;
        log.debug("test deleteAppointmentGroup");
        int expected = jamObj.findAllGroups().size();
        int result = jamObj.deleteAppointmentGroup(testId);
        assertEquals(--expected, jamObj.findAllGroups().size());
        log.debug("end testDeleteAppointmentGroup()");
    }

    /**
     * Test of testUpdateAppointmentGroup method, of class JamDAO.
     *
     * @throws java.sql.SQLException
     */
    @Test
    public void testUpdateAppointmentGroup() throws SQLException {
        log.debug("start testUpdateAppointmentGroup()");
        AppointmentGroupRecord group1 = jamObj.findAppointmentGroupByName("BUSINESS");
        group1.setColor("489af7");
        int updateResults = jamObj.update(group1);
        assertEquals(1, updateResults);
        log.debug("end  testUpdateAppointmentGroup()");

    }

}
