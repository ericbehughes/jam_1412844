package com.erichughes.tests;


import com.ericbehughes.database.JamDAO;
import com.ericbehughes.models.AppointmentRecord;
import com.ericbehughes.database.JamDbUtil;
import com.erichughes.testdatabase.SeedDatabase;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import junit.framework.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Unit test for AppointGroup/AppointmentGroup database
 *
 * @author eric hughes
 * @version 1.0
 */
public class TestAppointmentRecord {

    // This is my local MySQL server
    private final Logger log = LoggerFactory.getLogger(
            this.getClass().getName());
    private final JamDbUtil dbUtil = new JamDbUtil();
    private final JamDAO jamObj = dbUtil.getJamDAO();
    private final SeedDatabase seedDb = new SeedDatabase(dbUtil);
    

   
    /**
     * This will insert a record, retrieve the just inserted record and compare the inserted and retrieved objects
     *
     * @throws SQLException
     */
    //@Test
    public void testCreateAppointmentRecord() throws SQLException {
        
        AppointmentRecord record1 = new AppointmentRecord();

        record1.setTitle("An important title");
        record1.setLocation("A specific location");
        record1.setWholeDay(1);
        record1.setStartTimeStamp(Timestamp.valueOf("2017-09-06 09:00:00"));
        record1.setEndTimeStamp(Timestamp.valueOf("2017-09-06 09:00:00"));
        record1.setDetails("Some very long details");
        record1.setAppointmentGroup(1);
        record1.setAlarmReminderRequested(1);
        int result = jamObj.create(record1);
        log.debug(">>>>>>>>testAppointmentRecordCreate record1ID " + record1.getID());
        AppointmentRecord record2 = jamObj.findAppointmentByID(record1.getID());
        log.debug(">>>>>>>> record1  " + record1.toString());
        log.debug(">>>>>>>>>>>>>>>>> record2  " + record2.toString());
        assertEquals("A record was not created", record1, record2);
    }

    @Test
    public void testCreateAppointmentRecordFailure() throws SQLException {
        AppointmentRecord record1 = new AppointmentRecord();

        record1.setTitle("An important titleAn important titleAn important title");
        record1.setLocation("A specific location");
        record1.setWholeDay(1);
        record1.setStartTimeStamp(Timestamp.valueOf("2017-09-06 09:00:00"));
        record1.setEndTimeStamp(Timestamp.valueOf("2017-09-06 09:00:00"));
        record1.setDetails("Some very long details");
        record1.setAppointmentGroup(1);
        record1.setAlarmReminderRequested(1);
        int result = jamObj.create(record1);
        log.debug(">>>>>>>>>>>>>>>>>>>>>" + record1.getID());
        AppointmentRecord record2 = jamObj.findAppointmentByID(record1.getID());
        log.debug(">>>>>>>>>>>>>>>>> record1  " + record1.toString());
        log.debug(">>>>>>>>>>>>>>>>> record2  " + record2.toString());
    }

    @Test
    public void testfindAppointmentByID() throws SQLException {

        AppointmentRecord record1 = jamObj.findAppointmentByID(3);
        int expected = 3;

        log.debug(">>>>>>>>testfindAppointmentByID record1ID " + record1.getID());
        assertEquals(expected, record1.getID());
    }

    @Test (expected = SQLException.class)
    public void testfindAppointmentByIDFailure() throws SQLException {

        AppointmentRecord record1 = jamObj.findAppointmentByID(-1);
        int expected = 0;
        log.debug(">>>>>>>>testfindAppointmentByIDFailure record1ID " + record1.getID());
        fail("testFindAppointmentByIdFailure");
    }

    
    @Test 
    public void testfindAppointmentByTitle() throws SQLException {

        AppointmentRecord record1 = jamObj.findAppointmentByTitle("Finance");
        String expected = "Finance";

        log.debug(">>>>>>>>findAppointmentByTitle record1ID " + record1.getTitle());
        assertEquals(expected, record1.getTitle());
    }

    @Test (expected = SQLException.class)
    public void testfindAppointmentByTitleFailure() throws SQLException {

        // characters over 100
        AppointmentRecord record1 = jamObj.findAppointmentByTitle("not"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside"
                + "748392473892473489493293289743284723894738924782478923432inside");

        log.debug(">>>>>>>>findAppointmentByTitleFailure record1ID " + record1.getTitle());

        fail();
    }

    @Test
    public void testfindAllAppointmentsByDateAndTime() throws SQLException {

        //String str = "2017-09-14 18:00";
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.now().plusMinutes(120);
        //LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        List<AppointmentRecord> records = jamObj.findAllAppointmentsByDateAndTime(Timestamp.valueOf(dateTime));

        log.debug("Find all records by date and time");
        records.forEach((_item) -> {
            log.debug(">>>>>>>>testfindAllAppointmentsByDateAndTime record.toString() " + _item.toString());
        });

        assertEquals(0, records.size());
    }

    @Test (expected = SQLException.class)
    public void testfindAllAppointmentsByDateAndTimeFailure() throws SQLException {

        String str = "2017-09-14 -23:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        List<AppointmentRecord> records = jamObj.findAllAppointmentsByDateAndTime(Timestamp.valueOf(dateTime));

        log.debug("Find all records by date and time");
        records.forEach((_item) -> {
            log.debug(">>>>>>>>testfindAllAppointmentsByDateAndTimeFailure record.toString()" + _item.toString());
        });

       fail();

    }

    @Test
    public void testfindAllAppointmentsForGivenWeek() throws SQLException {
        String str = "2016-10-06 22:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        Timestamp t = Timestamp.valueOf(dateTime);
        List<AppointmentRecord> records = jamObj.findAllAppointmentsForGivenWeek(Timestamp.valueOf(dateTime));

        log.debug("Find all records for today start ");
        records.forEach((_item) -> {
            log.debug(">>>>>>>>testfindAllAppointmentsForGivenWeek record.toString()" + _item.toString());
        });

        assertEquals(4, records.size());

    }

    @Test (expected = SQLException.class)
    public void testfindAllAppointmentsForGivenWeekFailure() throws SQLException {
        String str = "-1920-09-06 22:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        Timestamp t = Timestamp.valueOf(dateTime);
        List<AppointmentRecord> records = jamObj.findAllAppointmentsForGivenWeek(Timestamp.valueOf(dateTime));

        log.debug("Find all records for today start ");
        records.forEach((_item) -> {
            log.debug(">>>>>>>>testfindAllAppointmentsForGivenWeekFailure record.toString()" + _item.toString());
        });
       fail();
        
    }


    @Test
    public void testfindAllAppointmentsForGivenMonth() throws SQLException {
        String str = "2016-09-16 22:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        Timestamp t = Timestamp.valueOf(dateTime);
        List<AppointmentRecord> records = jamObj.findAllAppointmentsForGivenMonth(Timestamp.valueOf(dateTime));

        log.debug("Find all records for given month ");
        records.forEach((_item) -> {
            log.debug(">>>>>>>>testfindAllAppointmentsForGivenMonth record.toString()" + _item.toString());
        });

        assertEquals(4, records.size());

    }
    
    @Test (expected = SQLException.class)
    public void testfindAllAppointmentsForGivenMonthFailure() throws SQLException {
        String str = "2016-09-16 22:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        Timestamp t = Timestamp.valueOf(dateTime);
        List<AppointmentRecord> records = jamObj.findAllAppointmentsForGivenMonth(Timestamp.valueOf(dateTime));

        log.debug("Find all records for given month ");
        records.forEach((_item) -> {
            log.debug(">>>>>>>>testfindAllAppointmentsForGivenMonthFailure record.toString()" + _item.toString());
        });

        fail();

    }

    @Test 
    public void testfindAllAppointmentsBetween() throws SQLException {

        //date 1
        String str = "2016-10-10 05:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime1 = LocalDateTime.parse(str, formatter);
        Timestamp t1 = Timestamp.valueOf(dateTime1);

        //date2
        str = "2016-10-17 22:00";
        LocalDateTime dateTime2 = LocalDateTime.parse(str, formatter);
        Timestamp t2 = Timestamp.valueOf(dateTime2);
        List<AppointmentRecord> records = jamObj.findAllAppointmentsBetween(Timestamp.valueOf(dateTime1), Timestamp.valueOf(dateTime2));

        log.debug("Find all records for between");
        records.forEach((_item) -> {
            log.debug(">>>>>>>>testfindAllAppointmentsBetween record.toString()" + _item.toString());
        });

        assertEquals(2, records.size());
    }

    //findAllAppointmentsBetween
    @Test (expected = SQLException.class)
    public void testfindAllAppointmentsBetweenFailure() throws SQLException {
        String str = "2017-09-06 00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        Timestamp t = Timestamp.valueOf(dateTime);
        List<AppointmentRecord> records = jamObj.findAllAppointmentsForGivenMonth(Timestamp.valueOf(dateTime));

        log.debug("Find all records for given month ");
        records.forEach((_item) -> {
            log.debug(">>>>>>>>testfindAllAppointmentsBetweenFailure record.toString()" + _item.toString());
        });

        fail();

    }

    /**
     * Test of deleteAppointment method, of class JamDAO.
     *
     * @throws java.sql.SQLException
     */
    @Test
    public void testDeleteAppointment() throws SQLException {

        int testId = 1;
        log.debug("test deleteAppointment");
        int expected = jamObj.findAllAppointments().size();
        int result = jamObj.deleteAppointment(testId);
        assertEquals(1, result);
        assertEquals(expected - 1, jamObj.findAllAppointments().size());

    }

    /**
     * Test of deleteAppointment method, of class JamDAO.
     *
     * @throws java.sql.SQLException
     */
    @Test
    public void testUpdateAppointment() throws SQLException {
        log.debug("test testUpdateAppointment");
        AppointmentRecord record1 = jamObj.findAppointmentByID(1);
        record1.setLocation("SomeNewLocation");
        int updateResults = jamObj.update(record1);

        assertEquals(1, updateResults);

    }



   

}
