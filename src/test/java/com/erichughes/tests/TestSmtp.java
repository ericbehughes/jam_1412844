/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erichughes.tests;

import com.ericbehughes.database.JamDAO;
import com.ericbehughes.models.Smtp;
import com.ericbehughes.database.JamDbUtil;
import java.sql.SQLException;
import java.util.List;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ehugh
 */
public class TestSmtp {
    // This is my local MySQL server

    private final Logger log = LoggerFactory.getLogger(
            this.getClass().getName());
    private final String url = "jdbc:mysql://localhost:3306/jam_1412844_db?autoReconnect=true&useSSL=false";
    private final String user = "root";
    private final String password = "dawson";
    private final JamDbUtil dbUtil = new JamDbUtil();
    private final JamDAO jamObj = dbUtil.getJamDAO();

    @Test
    public void testSmtpCreate() throws SQLException {

        Smtp s1 = new Smtp();
        s1.setUserName("someUsername");
        s1.setEmail("someUsername@email.com");
        s1.setPassword("encryptedPassword");
        s1.setUrl("https://www.gmail.com");
        s1.setPortNumber(11);
        s1.setIsDefault(1);
        s1.setReminderInterval(1);
        int result = jamObj.create(s1);
        log.debug(">>>>>>>>>>>>>>>>>>>>>" + s1.toString());
        Smtp s2 = jamObj.findSmtpById(2);
        Assert.assertEquals(s1, s2);

    }

    @Test(expected = SQLException.class)
    public void testSmtpCreateFailure() throws SQLException {

        Smtp s1 = new Smtp();
        s1.setUserName("someUsername");
        s1.setEmail("someUsername@email.com");
        s1.setPassword("encryptedPassword");
        s1.setUrl("https://www.gmail.comhttps://www.gmail.comhttps://www.gmail.comhttps://www.gmail.comhttps://www.gmail.comhttps://www.gmail.com");
        s1.setPortNumber(1133333333);
        s1.setIsDefault(3);
        int result = jamObj.create(s1);
        log.debug(">>>>>>>>>>>>>>>>>>>>>" + s1.toString());
        Smtp s2 = jamObj.findSmtpById(2);
        Assert.assertFalse(s1 == s2);

    }

    /**
     *
     * @throws SQLException
     */
    @Test
    public void testfindAllSmtp() throws SQLException {

        // build record 
        Smtp s1 = new Smtp();
        s1.setUserName("someUsername");
        s1.setEmail("someUsername@email.com");
        s1.setPassword("encryptedPassword");
        s1.setUrl("https://www.gmail.com");
        s1.setPortNumber(11);
        s1.setIsDefault(1);
        s1.setReminderInterval(10);
        int result = jamObj.create(s1);

        List<Smtp> list = jamObj.findAllSmtpConfigurations();

        log.debug(">>>>>>>>>>>>>>>>> Find all Smtp  " + list.toString());

        assertEquals(2, list.size());
    }

    @Test
    public void testfindAllSmtpFailure() throws SQLException {
        List<Smtp> list = jamObj.findAllSmtpConfigurations();

        log.debug(">>>>>>>>>>>>>>>>> Find all Smtp  " + list.toString());
        Assert.assertFalse(list.size() == 6);
    }

    /**
     * Test of deleteAppointment method, of class JamDAO.
     *
     * @throws java.sql.SQLException
     */
    @Test
    public void testDeleteSmtp() throws SQLException {

        Smtp s1 = new Smtp();
        s1.setUserName("someUsername");
        s1.setEmail("someUsername@email.com");
        s1.setPassword("encryptedPassword");
        s1.setUrl("https://www.gmail.com");
        s1.setPortNumber(11);
        s1.setIsDefault(1);
        s1.setReminderInterval(10);
        int result = jamObj.create(s1);

        int testId = 1;
        log.debug("test deleteSmtpGroup");
        int expected = jamObj.findAllSmtpConfigurations().size();
        int builtSmtp = jamObj.deleteSmtp(testId);
        assertEquals(1, result);
        assertEquals(expected - 1, jamObj.findAllSmtpConfigurations().size());

    }

    /**
     * Test of deleteAppointment method, of class JamDAO.
     *
     * @throws java.sql.SQLException
     */
    @Test
    public void testUpdateSmtpFailure() throws SQLException {
        log.debug("test deleteSmtpGroupFailure");
        Smtp s1 = jamObj.findSmtpById(1);
        s1.setUrl("https://outlook.");
        int updateResults = jamObj.update(s1);

        Assert.assertFalse(updateResults == 0);

    }

}
