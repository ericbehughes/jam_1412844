///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.erichughes.tests;
//
//import com.ericbehughes.database.JamDbUtil;
//import com.ericbehughes.database.JamDAO;
//import com.ericbehughes.email.JAMEmail;
//import com.ericbehughes.models.AppointmentRecord;
//import com.ericbehughes.models.Smtp;
//import java.sql.Timestamp;
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import org.junit.Test;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// *
// * @author ehugh
// */
//public class TestEmail {
//    
//        // This is my local MySQL server
//
//    private final Logger log = LoggerFactory.getLogger(
//            this.getClass().getName());
//    private final JamDbUtil dbUtil = new JamDbUtil();
//    private final JamDAO jamObj = dbUtil.getJamDAO();
//    
//
//    public TestEmail() {
//         
//         
//    }
//    
//    @Test
//    public void mailTest() {
//            
//            try {
//            createMail();
//            Timestamp t1 = Timestamp.valueOf(LocalDateTime.now().plusMinutes(117));
//            Timestamp t2 = Timestamp.valueOf(LocalDateTime.now().plusMinutes(123));
//            List<AppointmentRecord> mail = jamObj.findAllAppointmentsBetween(t1, t2);
//            sendEmail(mail);
//        } catch (Exception e) {
//        }
//            
//            
//    }
//    
//    private void createMail(){
//        AppointmentRecord ar1 = new AppointmentRecord();
//        ar1.setTitle("Dawson appointment1");
//        ar1.setLocation("Dawson");
//        ar1.setWholeDay(0);
//        ar1.setStartTimeStamp(Timestamp.valueOf(LocalDateTime.now().plusMinutes(120)));
//        ar1.setEndTimeStamp(Timestamp.valueOf(LocalDateTime.now().plusMinutes(180)));
//        ar1.setDetails("Some very long details");
//        ar1.setAppointmentGroup(1);
//        ar1.setAlarmReminderRequested(1);
//        
//        AppointmentRecord ar2 = new AppointmentRecord();
//        ar2.setTitle("Concordia appointment2");
//        ar2.setLocation("Concordia");
//        ar2.setWholeDay(0);
//        ar2.setStartTimeStamp(Timestamp.valueOf(LocalDateTime.now().plusMinutes(120)));
//        ar2.setEndTimeStamp(Timestamp.valueOf(LocalDateTime.now().plusMinutes(180)));
//        ar2.setDetails("Some very long details");
//        ar2.setAppointmentGroup(1);
//        ar2.setAlarmReminderRequested(0);
//        
//        AppointmentRecord ar3 = new AppointmentRecord();
//        ar3.setTitle("Bellville appointment3");
//        ar3.setLocation("Belleville");
//        ar3.setWholeDay(0);
//        ar3.setStartTimeStamp(Timestamp.valueOf(LocalDateTime.now().plusMinutes(120)));
//        ar3.setEndTimeStamp(Timestamp.valueOf(LocalDateTime.now().plusMinutes(180)));
//        ar3.setDetails("Some very long details");
//        ar3.setAppointmentGroup(1);
//        ar3.setAlarmReminderRequested(1);
//        try {
//            
//            jamObj.create(ar1);
//            jamObj.create(ar2);
//            jamObj.create(ar3);
//            
//            
//        } catch (Exception e) {
//        }
//        
//         
//    }
//    
//    public void sendEmail(List<AppointmentRecord> mailToSend){
//        try {
//            Smtp s = jamObj.findDefaultSmtp();
//            JAMEmail je = new JAMEmail(s);
//        for (AppointmentRecord appointmentRecord : mailToSend) {
//            //send email
//            je.perform(appointmentRecord);
//        }
//        } catch (Exception e) {
//        }
//        
//        
//        
//    }
//          
//    
//    
//    
//}
