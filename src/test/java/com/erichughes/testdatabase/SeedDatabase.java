/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erichughes.testdatabase;

import com.ericbehughes.database.JamDbUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ehugh
 */
public class SeedDatabase {
        private final Logger log = LoggerFactory.getLogger(
            this.getClass().getName());
    private JamDbUtil dbUtil;
    public SeedDatabase(JamDbUtil util) {
        log.debug("this.util" + util);
        this.dbUtil = util;
        seedDatabase();
    }

    

 /**
     * This routine recreates the database before every test. This makes sure that a destructive test will not interfere with any other test. Does not support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, the lead Arquillian developer at JBoss
     */
    //@Before

    private void seedDatabase() {
        log.info("Seeding Database");
        final String seedDataScript = loadAsString("./CreateJamTables.sql");
        log.info("found seedDataSCript");
        try (Connection connection = DriverManager
                .getConnection(this.dbUtil.getCredentials().getUrl(), this.dbUtil.getCredentials().getUser(), this.dbUtil.getCredentials().getPassword())) {
            log.info("Seeding Database connection succedded");
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabase method
     */
    private String loadAsString(final String path) {
        log.info("start load as string  "  + path);
        
        try (InputStream inputStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }

    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }

    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");

    }    
}
