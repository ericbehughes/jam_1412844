-- DROP DATABASE IF EXISTS JAM_1412844_DB;
-- CREATE DATABASE JAM_1412844_DB;

USE JAM_1412844_DB;

DROP TABLE IF EXISTS APPOINTMENTRECORD;


CREATE TABLE APPOINTMENTRECORD
(
    ID INT NOT NULL AUTO_INCREMENT,
    TITLE VARCHAR(100) NOT NULL,
    LOCATION VARCHAR(100) NOT NULL,
    WHOLEDAY CHAR(1) not NULL default '0',
    STARTTIME TIMESTAMP NOT NULL ,
    ENDTIME TIMESTAMP NOT NULL,
    DETAILS VARCHAR(100) NULL,
    APPOINTMENTGROUP INT NOT NULL,  
    ALARMREMINDERREQUESTED CHAR(1) not NULL default '0',
    CONSTRAINT APPOINTMENTRECORD_PK PRIMARY KEY (ID)
) ENGINE=INNODB;

DROP TABLE IF EXISTS APPOINTMENTGROUPRECORD;

CREATE TABLE APPOINTMENTGROUPRECORD
(
    ID INT NOT NULL AUTO_INCREMENT,
    GROUPNAME VARCHAR(100) NOT NULL,
    COLOR CHAR(6) NOT NULL,
    CONSTRAINT APPOINTMENTGROUPRECORD_PK PRIMARY KEY (ID)
) ENGINE=INNODB;

DROP TABLE IF EXISTS SMTP;

CREATE TABLE SMTP
(
    ID INT NOT NULL AUTO_INCREMENT,
    USERNAME VARCHAR(100) NOT NULL,
    EMAIL VARCHAR(100) NOT NULL default 'hues.business@gmail.com',
    PASSWORD VARCHAR(100) NOT NULL,
    URL VARCHAR(100) NOT NULL,
    PORTNUMBER INT NOT NULL DEFAULT '465',
    ISDEFAULT CHAR(1) DEFAULT '0',
    REMINDERINTERVAL INT default 120,
    CONSTRAINT SMTP_PK PRIMARY KEY (ID)
) ENGINE=INNODB;



insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (1, 'Finance', '10 Glacier Hill Center', 0, '2017-06-25 18:21:00', '2017-06-25 18:51:00', 'e-enable vertical web-readiness', 3, 1 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (2, 'Energy', '7332 Grayhawk Point', 0, '2017-10-03 10:52:00', '2017-10-03 11:22:00', 'recontextualize killer channels', 1, 1 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (3, 'Miscellaneous', '5486 West Park', 1, '2017-07-23 00:00:00', '2017-07-23 23:59:00', 'recontextualize ubiquitous networks', 2, 0  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (4, 'Transportation', '15949 Hanover Circle', 0, '2017-10-11 08:12:00', '2017-10-11 08:42:00', 'reintermediate visionary experiences', 3, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (5, 'Consumer Services', '5924 Marcy Place', 1, '2017-11-16 00:00:00', '2017-11-16 23:59:00', 'deliver sticky networks', 3, 0  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (6, 'Transportation', '817 Summer Ridge Park', 1, '2017-09-06 00:00:00', '2017-09-06 23:59:00', 'cultivate value-added markets', 2, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (7, 'Public Utilities', '57 Mccormick Terrace', 1, '2017-08-22 00:00:00', '2017-08-22 23:59:00', 'evolve next-generation eyeballs', 1, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (8, 'Miscellaneous', '5392 Morning Court', 0, '2017-06-15 15:02:00', '2017-06-15 15:32:00', 'generate open-source solutions', 2, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (9, 'Technology', '3438 Burning Wood Avenue', 1, '2017-08-08 00:00:00', '2017-08-08 23:59:00', 'seize clicks-and-mortar platforms', 2, 0  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (10, 'Consumer Non-Durables', '21 School Lane', 0, '2017-10-25 09:29:00', '2017-10-25 09:59:00', 'matrix magnetic e-business', 5, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (11, 'Consumer Non-Durables', '89 Eastlawn Place', 1, '2017-12-30 00:00:00', '2017-12-30 23:59:00', 'envisioneer cutting-edge supply-chains', 5, 0  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (12, 'Energy', '8327 Texas Trail', 1, '2017-10-08 00:00:00', '2017-10-08 23:59:00', 'morph web-enabled methodologies', 3, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (13, 'Health Care', '8406 Superior Avenue', 0, '2017-03-28 01:09:00', '2017-03-28 01:39:00', 'brand one-to-one e-services', 3, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (14, 'Health Care', '5552 Morrow Drive', 1, '2017-06-29 00:00:00', '2017-06-29 23:59:00', 'enable 24/7 infomediaries', 1, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (15, 'Health Care', '44 Porter Center', 0, '2017-09-16 14:28:00', '2017-09-16 14:58:00', 'innovate leading-edge mindshare', 3, 1);
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (16, 'Important', '9677 Cordelia Terrace', 0, '2017-11-04 12:14:00', '2017-11-04 12:44:00', 'leverage web-enabled infomediaries', 3, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (17, 'Important', '7650 Elmside Plaza', 1, '2017-10-24 00:00:00', '2017-10-24 23:59:00', 'monetize clicks-and-mortar communities', 1, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (18, 'Consumer Durables', '7585 Monica Parkway', 0, '2017-12-14 03:16:00', '2017-12-14 03:46:00', 'engage dot-com infrastructures', 4, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (19, 'Technology', '96 Schiller Junction', 1, '2017-10-26 00:00:00', '2017-10-26 23:59:00', 'deploy value-added experiences', 5, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (20, 'Consumer Non-Durables', '03 Northport Park', 0, '2017-10-05 18:22:00', '2017-10-05 18:52:00', 'architect bricks-and-clicks paradigms', 5, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (21, 'Capital Goods', '8 Vahlen Junction', 0, '2017-07-07 02:05:00', '2017-07-07 02:35:00', 'whiteboard collaborative initiatives', 2, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (22, 'Finance', '6916 Sundown Drive', 0, '2017-03-20 06:29:00', '2017-03-20 06:59:00', 'drive B2B interfaces', 5, 0  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (23, 'Health Care', '23 Village Green Plaza', 0, '2017-03-06 18:12:00', '2017-03-06 18:42:00', 'e-enable frictionless mindshare', 4, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (24, 'Consumer Non-Durables', '8 Bashford Way', 1, '2017-01-05 00:00:00', '2017-01-05 23:59:00', 'utilize cross-platform e-markets', 4, 1 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (25, 'Health Care', '1 Monument Trail', 1, '2017-09-10 00:00:00', '2017-09-10 23:59:00', 'visualize impactful partnerships', 1, 0  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (26, 'Consumer Durables', '393 Riverside Hill', 1, '2017-12-09 00:00:00', '2017-12-09 23:59:00', 'brand killer portals', 5, 0  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (27, 'Public Utilities', '333 Springs Road', 0, '2017-10-10 20:21:00', '2017-10-10 20:51:00', 'embrace sticky bandwidth', 3, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (28, 'Technology', '9 Rusk Avenue', 0, '2017-10-30 13:41:00', '2017-10-30 14:11:00', 'leverage 24/365 web services', 5, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (29, 'Finance', '66 Coleman Junction', 0, '2017-02-03 12:54:00', '2017-02-03 13:24:00', 'seize vertical solutions', 1, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (30, 'Health Care', '064 Cardinal Place', 0, '2017-08-03 19:58:00', '2017-08-03 20:28:00', 'engage granular e-business', 4, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (31, 'Consumer Non-Durables', '937 Hintze Center', 1, '2017-03-15 00:00:00', '2017-03-15 23:59:00', 'whiteboard holistic schemas', 4, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (32, 'Important', '4 Farwell Place', 1, '2017-08-27 00:00:00', '2017-08-27 23:59:00', 'evolve world-class deliverables', 3, 1 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (33, 'Finance', '9032 Arrowood Terrace', 0, '2017-08-05 04:26:00', '2017-08-05 04:56:00', 'enhance front-end initiatives', 3, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (34, 'Important', '4 Harbort Center', 1, '2017-08-16 00:00:00', '2017-08-16 23:59:00', 'synergize mission-critical eyeballs', 5, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (35, 'Important', '3 Grasskamp Center', 0, '2017-10-26 18:07:00', '2017-10-26 18:37:00', 'brand integrated bandwidth', 4, 0  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (36, 'Important', '92 Calypso Trail', 0, '2017-07-05 10:21:00', '2017-07-05 10:51:00', 'innovate sexy deliverables', 2, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (37, 'Consumer Non-Durables', '8872 Bashford Drive', 1, '2017-07-30 00:00:00', '2017-07-30 23:59:00', 'leverage synergistic technologies', 3, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (38, 'Health Care', '30192 Cordelia Center', 1, '2017-11-17 00:00:00', '2017-11-17 23:59:00', 'productize web-enabled content', 5, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (39, 'Health Care', '69 Westerfield Junction', 1, '2017-10-27 00:00:00', '2017-10-27 23:59:00', 'synergize open-source methodologies', 1, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (40, 'Technology', '00 Commercial Road', 0, '2017-10-03 01:40:00', '2017-10-03 02:10:00', 'reintermediate robust bandwidth', 1, 0);
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (41, 'Health Care', '210 Sage Place', 1, '2017-10-30 00:00:00', '2017-10-30 23:59:00', 'benchmark sexy infomediaries', 3, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (42, 'Important', '243 Westerfield Place', 1, '2017-09-19 00:00:00', '2017-09-19 23:59:00', 'drive efficient models', 1, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (43, 'Consumer Services', '887 Elgar Place', 1, '2017-10-12 00:00:00', '2017-10-12 23:59:00', 'innovate virtual infrastructures', 2, 0  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (44, 'Finance', '506 Bayside Alley', 0, '2017-08-14 06:04:00', '2017-08-14 06:34:00', 'grow transparent e-commerce', 1, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (45, 'Important', '92 Sachs Court', 1, '2017-01-12 00:00:00', '2017-01-12 23:59:00', 'e-enable cutting-edge infomediaries', 5, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (46, 'Public Utilities', '33079 Westport Circle', 0, '2017-10-05 07:45:00', '2017-10-05 08:15:00', 'matrix customized networks', 2, 1  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (47, 'Capital Goods', '4 Esch Street', 0, '2017-10-19 16:09:00', '2017-10-19 16:39:00', 'cultivate impactful communities', 4, 0  );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (48, 'Basic Industries', '611 Troy Junction', 0, '2017-09-18 21:19:00', '2017-09-18 21:49:00', 'cultivate bricks-and-clicks web services', 1, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (49, 'Finance', '02079 Stephen Street', 0, '2017-12-21 11:24:00', '2017-12-21 11:54:00', 'productize seamless schemas', 1, 0 );
insert into APPOINTMENTRECORD (id, title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values (50, 'Health Care', '1509 Texas Circle', 0, '2017-09-14 18:00:00', '2017-09-14 19:00:00', 'unleash back-end web-readiness', 3, 0  );

insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ('Finance', '10 Glacier Hill Center', 0, '2018-06-25 18:21:00', '2018-06-25 18:51:00', 'e-enable vertical web-readiness', 3, 1 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ('Energy', '7332 Grayhawk Point', 0, '2018-10-03 10:52:00', '2018-10-03 11:22:00', 'recontextualize killer channels', 1, 1 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ('Miscellaneous', '5486 West Park', 1, '2018-07-23 00:00:00', '2018-07-23 23:59:00', 'recontextualize ubiquitous networks', 2, 0  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ('Transportation', '15949 Hanover Circle', 0, '2018-04-11 08:12:00', '2018-04-11 08:42:00', 'reintermediate visionary experiences', 3, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ('Consumer Services', '5924 Marcy Place', 1, '2018-11-16 00:00:00', '2018-11-16 23:59:00', 'deliver sticky networks', 3, 0  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ('Transportation', '817 Summer Ridge Park', 1, '2018-09-06 00:00:00', '2018-09-06 23:59:00', 'cultivate value-added markets', 2, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ('Public Utilities', '57 Mccormick Terrace', 1, '2018-08-22 00:00:00', '2018-08-22 23:59:00', 'evolve next-generation eyeballs', 1, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ('Miscellaneous', '5392 Morning Court', 0, '2018-06-15 15:02:00', '2018-06-15 15:32:00', 'generate open-source solutions', 2, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ('Technology', '3438 Burning Wood Avenue', 1, '2018-08-08 00:00:00', '2018-08-08 23:59:00', 'seize clicks-and-mortar platforms', 2, 0  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Consumer Non-Durables', '21 School Lane', 0, '2018-10-15 09:29:00', '2018-10-15 09:59:00', 'matrix magnetic e-business', 5, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Consumer Non-Durables', '89 Eastlawn Place', 1, '2018-12-30 00:00:00', '2018-12-30 23:59:00', 'envisioneer cutting-edge supply-chains', 5, 0  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Energy', '8327 Texas Trail', 1, '2018-10-08 00:00:00', '2018-10-08 23:59:00', 'morph web-enabled methodologies', 3, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Health Care', '8406 Superior Avenue', 0, '2018-03-28 01:09:00', '2018-03-28 01:39:00', 'brand one-to-one e-services', 3, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Health Care', '5552 Morrow Drive', 1, '2018-06-29 00:00:00', '2018-06-29 23:59:00', 'enable 24/7 infomediaries', 1, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Health Care', '44 Porter Center', 0, '2018-09-16 14:28:00', '2018-09-16 14:58:00', 'innovate leading-edge mindshare', 3, 1);
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Important', '9677 Cordelia Terrace', 0, '2018-11-04 12:14:00', '2018-11-04 12:44:00', 'leverage web-enabled infomediaries', 3, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Important', '7650 Elmside Plaza', 1, '2018-05-24 00:00:00', '2018-05-24 23:59:00', 'monetize clicks-and-mortar communities', 1, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Consumer Durables', '7585 Monica Parkway', 0, '2018-12-14 03:16:00', '2018-12-14 03:46:00', 'engage dot-com infrastructures', 4, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Technology', '96 Schiller Junction', 1, '2018-04-26 00:00:00', '2018-04-26 23:59:00', 'deploy value-added experiences', 5, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Consumer Non-Durables', '03 Northport Park', 0, '2018-10-05 18:22:00', '2018-10-05 18:52:00', 'architect bricks-and-clicks paradigms', 5, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Capital Goods', '8 Vahlen Junction', 0, '2018-07-07 02:05:00', '2018-07-07 02:35:00', 'whiteboard collaborative initiatives', 2, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Finance', '6916 Sundown Drive', 0, '2018-03-20 06:29:00', '2018-03-20 06:59:00', 'drive B2B interfaces', 5, 0  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Health Care', '23 Village Green Plaza', 0, '2018-03-06 18:12:00', '2018-03-06 18:42:00', 'e-enable frictionless mindshare', 4, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Consumer Non-Durables', '8 Bashford Way', 1, '2018-01-05 00:00:00', '2018-01-05 23:59:00', 'utilize cross-platform e-markets', 4, 1 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Health Care', '1 Monument Trail', 1, '2018-09-10 00:00:00', '2018-09-10 23:59:00', 'visualize impactful partnerships', 1, 0  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Consumer Durables', '393 Riverside Hill', 1, '2018-12-09 00:00:00', '2018-12-09 23:59:00', 'brand killer portals', 5, 0  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Public Utilities', '333 Springs Road', 0, '2018-10-10 20:21:00', '2018-10-10 20:51:00', 'embrace sticky bandwidth', 3, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Technology', '9 Rusk Avenue', 0, '2018-04-30 13:41:00', '2018-04-30 14:11:00', 'leverage 24/365 web services', 5, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Finance', '66 Coleman Junction', 0, '2018-02-03 12:54:00', '2018-02-03 13:24:00', 'seize vertical solutions', 1, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Health Care', '064 Cardinal Place', 0, '2018-08-03 19:58:00', '2018-08-03 20:28:00', 'engage granular e-business', 4, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Consumer Non-Durables', '937 Hintze Center', 1, '2018-03-15 00:00:00', '2018-03-15 23:59:00', 'whiteboard holistic schemas', 4, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Important', '4 Farwell Place', 1, '2018-08-27 00:00:00', '2018-08-27 23:59:00', 'evolve world-class deliverables', 3, 1 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Finance', '9032 Arrowood Terrace', 0, '2018-08-05 04:26:00', '2018-08-05 04:56:00', 'enhance front-end initiatives', 3, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Important', '4 Harbort Center', 1, '2018-08-16 00:00:00', '2018-08-16 23:59:00', 'synergize mission-critical eyeballs', 5, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Important', '3 Grasskamp Center', 0, '2018-10-16 18:07:00', '2018-10-16 18:37:00', 'brand integrated bandwidth', 4, 0  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Important', '92 Calypso Trail', 0, '2018-07-05 10:21:00', '2018-07-05 10:51:00', 'innovate sexy deliverables', 2, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Consumer Non-Durables', '8872 Bashford Drive', 1, '2018-07-30 00:00:00', '2018-07-30 23:59:00', 'leverage synergistic technologies', 3, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Health Care', '30192 Cordelia Center', 1, '2018-11-17 00:00:00', '2018-11-17 23:59:00', 'productize web-enabled content', 5, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Health Care', '69 Westerfield Junction', 1, '2018-10-01 00:00:00', '2018-10-01 23:59:00', 'synergize open-source methodologies', 1, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Technology', '00 Commercial Road', 0, '2018-10-03 01:40:00', '2018-10-03 02:10:00', 'reintermediate robust bandwidth', 1, 0);
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Health Care', '210 Sage Place', 1, '2018-04-30 00:00:00', '2018-04-30 23:59:00', 'benchmark sexy infomediaries', 3, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Important', '243 Westerfield Place', 1, '2018-09-19 00:00:00', '2018-09-19 23:59:00', 'drive efficient models', 1, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Consumer Services', '887 Elgar Place', 1, '2018-04-12 00:00:00', '2018-04-12 23:59:00', 'innovate virtual infrastructures', 2, 0  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Finance', '506 Bayside Alley', 0, '2018-08-14 06:04:00', '2018-08-14 06:34:00', 'grow transparent e-commerce', 1, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Important', '92 Sachs Court', 1, '2018-01-12 00:00:00', '2018-01-12 23:59:00', 'e-enable cutting-edge infomediaries', 5, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Public Utilities', '33079 Westport Circle', 0, '2018-05-05 07:45:00', '2018-05-05 08:15:00', 'matrix customized networks', 2, 1  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Capital Goods', '4 Esch Street', 0, '2018-04-19 16:09:00', '2018-04-19 16:39:00', 'cultivate impactful communities', 4, 0  );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Basic Industries', '611 Troy Junction', 0, '2018-09-18 21:19:00', '2018-09-18 21:49:00', 'cultivate bricks-and-clicks web services', 1, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Finance', '02079 Stephen Street', 0, '2018-12-21 11:24:00', '2018-12-21 11:54:00', 'productize seamless schemas', 1, 0 );
insert into APPOINTMENTRECORD ( title, location, wholeday, starttime, endtime, details, appointmentgroup, alarmreminderrequested) values ( 'Health Care', '1509 Texas Circle', 0, '2018-09-14 18:00:00', '2018-09-14 19:00:00', 'unleash back-end web-readiness', 3, 0  );


insert into APPOINTMENTGROUPRECORD (GROUPNAME, COLOR) values ('BUSINESS', '489ac9');
insert into APPOINTMENTGROUPRECORD (GROUPNAME, COLOR) values ('EDUCATION', 'ee4e5a');
insert into APPOINTMENTGROUPRECORD (GROUPNAME, COLOR) values ('DOCTOR', '41ba7d');
insert into APPOINTMENTGROUPRECORD (GROUPNAME, COLOR) values ('PERSONAL', '000000');
insert into APPOINTMENTGROUPRECORD (GROUPNAME, COLOR) values ('OTHER', 'f4c950');


INSERT INTO smtp (USERNAME, EMAIL, PASSWORD, URL, PORTNUMBER, ISDEFAULT) VALUES ('someUser', 'fydranttrading@gmail.com', 'jamEmailTest', 'someUrl', 487, 1);



