/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ericbehughes.database;

import com.ericbehughes.models.AppointmentGroupRecord;
import com.ericbehughes.models.AppointmentRecord;
import com.ericbehughes.models.DatabaseCredentials;
import com.ericbehughes.models.Smtp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ehugh
 */
public class JamDAO implements IJamDAO {

    private final Logger log = LoggerFactory.getLogger(
            this.getClass().getName());
    private final DatabaseCredentials credentials;

    public JamDAO(DatabaseCredentials credentials) {
        log.debug("begin JAMDAO constructor");

        this.credentials = credentials;
        log.debug("end JAMDAO constructor");
    }

    @Override
    public int create(AppointmentRecord appointment) throws SQLException {
        log.debug("start create appointment record");
        String createQuery = "INSERT INTO APPOINTMENTRECORD (title, location, wholeday, starttime, ENDTIME, details, appointmentgroup, alarmreminderrequested ) values (?, ?, ?, ?, ?, ?, ?, ?)";
        int result;
        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, appointment.getTitle());
            ps.setString(2, appointment.getLocation());
            ps.setInt(3, appointment.getWholeDay());
            ps.setTimestamp(4, appointment.getStartTime());
            ps.setTimestamp(5, appointment.getEndTime());
            ps.setString(6, appointment.getDetails());
            ps.setInt(7, appointment.getAppointmentGroup());
            ps.setInt(8, appointment.getAlarmReminderRequested());
            result = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            int recordNum = -1;

            if (rs.next()) {
                recordNum = rs.getInt(1);

            }
            appointment.setID(recordNum);
            log.debug("end create appointment record" + appointment.toString());
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }

        return result;

    }

    @Override
    public int create(AppointmentGroupRecord agr) throws SQLException {
        log.debug("start create appointmentgrouprecord");
        int result;
        String createQuery = "INSERT into APPOINTMENTGROUPRECORD (GROUPNAME, COLOR) values(?,?)";
        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, agr.getGroupName());
            ps.setString(2, agr.getColor());
            result = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            int recordNum = -1;

            if (rs.next()) {
                recordNum = rs.getInt(1);

            }
            agr.setID(recordNum);

            log.debug("end create appointmentgrouprecord");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return result;

    }

    @Override
    public int create(Smtp s) throws SQLException {
        log.debug("start create smtp");
        int result;
        String createQuery = "insert into SMTP (username, email, password, url, portnumber, isdefault,reminderinterval) values (?,?,?,?,?,?,?)";
        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, s.getUserName());
            ps.setString(2, s.getEmail());
            ps.setString(3, s.getPassword());
            ps.setString(4, s.getUrl());
            ps.setInt(5, s.getPortNumber());
            ps.setInt(6, s.getIsDefault());
            ps.setInt(7, s.getReminderInterval());
            result = ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            int recordNum = -1;

            if (rs.next()) {
                recordNum = rs.getInt(1);

            }
            s.setID(recordNum);

            log.debug("end create appointmentgrouprecord");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return result;

    }

    @Override
    public AppointmentRecord findAppointmentByID(int id) throws SQLException {
        log.debug("start findAppointmentByID");
        AppointmentRecord ar = new AppointmentRecord();
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE ID = ?";
        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    ar = makeAppointmentRecord(resultSet);

                }

            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findAppointmentByID");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }

        return ar;
    }

    public AppointmentRecord findNextByID(AppointmentRecord ar) throws SQLException {

        log.debug("inside find by nect id JAM method" + ar.toString());
        String selectQuery = "SELECT * from appointmentrecord WHERE ID = (SELECT MIN(ID) from appointmentrecord WHERE ID > ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = getConnection();
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, ar.getID());
            log.debug("record id " + ar.getID());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    log.debug("inside result set " + ar.getID());
                    createBoundAppointmentRecord(resultSet, ar);
                }
            }
        }
        log.debug("Found " + ar.toString());
        //log.info("Found " + ar.getId());
        return ar;
    }

    /**
     * Retrieve the previous record from the given table based on the current
     * primary key using a bound bean
     *
     */
    public AppointmentRecord findPrevByID(AppointmentRecord ar) throws SQLException {

        String selectQuery = "SELECT * FROM APPOINTMENTRECORD WHERE ID = (SELECT MAX(ID) from appointmentrecord WHERE ID < ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = getConnection();
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, ar.getID());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundAppointmentRecord(resultSet, ar);
                }
            }
        }
        //log.info("Found " + fishData.getId());
        return ar;
    }

    public AppointmentGroupRecord findNextByID(AppointmentGroupRecord agr) throws SQLException {

        //log.debug("inside find by nextid appointmentgrouprecord" + agr.getID());
        String selectQuery = "SELECT * from AppointmentGroupRecord WHERE ID = (SELECT MIN(ID) from AppointmentGroupRecord WHERE ID > ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = getConnection();
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, agr.getID());
            //log.debug("record id " + agr.getID());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    //log.debug("inside result set " + agr.getID());
                    createBoundAppointmentGroupRecord(resultSet, agr);
                }
            }
        }
        log.debug("Found " + agr.toString());
        //log.info("Found " + ar.getId());
        return agr;
    }

    /**
     * Retrieve the previous record from the given table based on the current
     * primary key using a bound bean
     *
     */
    public AppointmentGroupRecord findPrevByID(AppointmentGroupRecord agr) throws SQLException {

        String selectQuery = "SELECT * FROM appointmentgrouprecord WHERE ID = (SELECT MAX(ID) from appointmentgrouprecord WHERE ID < ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = getConnection();
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, agr.getID());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundAppointmentGroupRecord(resultSet, agr);
                }
            }
        }
        //log.info("Found " + fishData.getId());
        return agr;
    }

    public Smtp findNextByID(Smtp s) throws SQLException {

        log.debug("inside find by nect id JAM method" + s.toString());
        String selectQuery = "SELECT * from smtp WHERE ID = (SELECT MIN(ID) from smtp WHERE ID > ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = getConnection();
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, s.getID());
            log.debug("record id " + s.getID());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    log.debug("inside result set " + s.getID());
                    createBoundSmtpRecord(resultSet, s);
                }
            }
        }
        log.debug("Found " + s.toString());
        return s;
    }

    /**
     * Retrieve the previous record from the given table based on the current
     * primary key using a bound bean
     *
     */
    public Smtp findPrevByID(Smtp s) throws SQLException {

        String selectQuery = "SELECT * FROM smtp WHERE ID = (SELECT MAX(ID) from smtp WHERE ID < ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = getConnection();
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, s.getID());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    createBoundSmtpRecord(resultSet, s);
                }
            }
        }
        //log.info("Found " + fishData.getId());
        return s;
    }

    
    
    
    @Override
    public AppointmentGroupRecord findAppointmentGroupRecordById(int id) throws SQLException {
        log.debug("start findAppointmentGroupRecordById");
        AppointmentGroupRecord agr = new AppointmentGroupRecord();
        String query = "SELECT * FROM APPOINTMENTGROUPRECORD WHERE ID = ?";
        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query);) {

            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                log.debug("start result set");
                if (resultSet.next()) {

                    log.debug("start resultset.next()");
                    agr = makeAppointmentGroupRecord(resultSet);

                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findAppointmentGroupRecordById");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return agr;
    }

    @Override
    public AppointmentRecord findAppointmentByTitle(String title) throws SQLException {
        log.debug("start findAppointmentByTitle");
        AppointmentRecord ar = new AppointmentRecord();
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE TITLE = ?;";

        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, title);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    ar = makeAppointmentRecord(resultSet);

                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("start findAppointmentByTitle");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return ar;

    }

    @Override
    public List<AppointmentRecord> findAllAppointmentsByDateAndTime(Timestamp t) throws SQLException {
        log.debug("start findAllAppointmentsByDateAndTime");
        List<AppointmentRecord> list = new ArrayList<>();
        AppointmentRecord ar = new AppointmentRecord();
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE STARTTIME = ?;";

        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            ps.setTimestamp(1, t);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    ar = makeAppointmentRecord(resultSet);
                    list.add(ar);
                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findAllAppointmentsByDateAndTime");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return list;
    }

    @Override
    public List<AppointmentRecord> findAllAppointmentsForGivenWeek(Timestamp t) throws SQLException {
        log.debug("start findAllAppointmentsForGivenWeek");
        List<AppointmentRecord> list = new ArrayList<>();
        AppointmentRecord ar = new AppointmentRecord();
        String query
                = "SELECT * FROM APPOINTMENTRECORD where STARTTIME BETWEEN DATE_ADD(?, INTERVAL(1-DAYOFWEEK(?)) DAY)"
                + "AND DATE_SUB(TIMESTAMP(DATE_ADD(DATE_ADD(?, INTERVAL 1 DAY), INTERVAL(7-DAYOFWEEK(?)) DAY)), INTERVAL 1 MINUTE)";

        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            ps.setTimestamp(1, t);
            ps.setTimestamp(2, t);
            ps.setTimestamp(3, t);
            ps.setTimestamp(4, t);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    ar = makeAppointmentRecord(resultSet);
                    list.add(ar);
                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findAllAppointmentsForGivenWeek");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return list;
    }

    @Override
    public List<AppointmentRecord> findAllAppointmentsForGivenMonth(Timestamp t) throws SQLException {
        log.debug("start findAllAppointmentsForGivenMonth");
        List<AppointmentRecord> list = new ArrayList<>();
        AppointmentRecord ar;
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE STARTTIME "
                + "BETWEEN  TIMESTAMP(CAST(DATE_FORMAT(? ,'%Y-%m-01') as DATE)) and LAST_DAY(?);";

        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            ps.setTimestamp(1, t);
            ps.setTimestamp(2, t);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    ar = makeAppointmentRecord(resultSet);
                    list.add(ar);
                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findAllAppointmentsForGivenMonth");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return list;
    }

    @Override
    public List<AppointmentGroupRecord> findAllGroups() throws SQLException {
        log.debug("start findAllGroups");
        List<AppointmentGroupRecord> list = new ArrayList<>();
        AppointmentGroupRecord agr;
        String query = "SELECT * FROM APPOINTMENTGROUPRECORD;";

        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    agr = makeAppointmentGroupRecord(resultSet);
                    list.add(agr);
                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findAllGroups");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return list;
    }

    @Override
    public List<Smtp> findAllSmtpConfigurations() throws SQLException {
        log.debug("start findAllSmtpConfigurations() ");
        List<Smtp> list = new ArrayList<>();
        Smtp smtp;
        String query = "SELECT * FROM SMTP;";

        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    smtp = makeSmptRecord(resultSet);
                    list.add(smtp);
                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findAllSmtpConfigurations() ");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return list;
    }

    @Override
    public AppointmentGroupRecord findAppointmentGroupByName(String name) throws SQLException {
        log.debug("start findAppointmentGroupByName");
        AppointmentGroupRecord agr = new AppointmentGroupRecord();
        String query = "SELECT * FROM APPOINTMENTGROUPRECORD WHERE GROUPNAME = ?";

        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    agr = makeAppointmentGroupRecord(resultSet);

                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findAppointmentGroupByName");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return agr;
    }

    @Override
    public List<AppointmentRecord> findAllAppointments() throws SQLException {
        log.debug("start findAllAppointments");
        List<AppointmentRecord> list = new ArrayList<>();
        AppointmentRecord ar = new AppointmentRecord();
        String query = "SELECT * FROM APPOINTMENTRECORD;";

        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {

            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    ar = makeAppointmentRecord(resultSet);
                    list.add(ar);
                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findAllAppointments");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return list;
    }

    @Override
    public List<AppointmentRecord> findAllAppointmentsBetween(Timestamp t1, Timestamp t2) throws SQLException {
        log.debug("start findAllAppointmentsBetween");
        List<AppointmentRecord> list = new ArrayList<>();
        AppointmentRecord ar = new AppointmentRecord(); // 
        String query = "SELECT * FROM APPOINTMENTRECORD WHERE STARTTIME BETWEEN ? and ?;";

        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            ps.setTimestamp(1, t1);
            ps.setTimestamp(2, t2);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    ar = makeAppointmentRecord(resultSet);
                    list.add(ar);
                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findAllAppointmentsBetween");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return list;
    }

    @Override
    public Smtp findSmtpById(int id) throws SQLException {
        log.debug("start findSmtpById");
        String query = "SELECT * FROM SMTP WHERE ID = ?";
        Smtp smtp = new Smtp();

        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    smtp = makeSmptRecord(resultSet);

                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findSmtpById");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return smtp;
    }

    @Override
    public int update(AppointmentRecord ar) throws SQLException {
        log.debug("start update(AppointmentRecord ar)" + ar.toString());
        AppointmentRecord temp = findAppointmentByID(ar.getID());
        String query = "UPDATE APPOINTMENTRECORD SET title = ?, location= ?, wholeday= ?, starttime= ?, endtime= ?, details= ?, appointmentgroup= ?, alarmreminderrequested= ? WHERE id = ?";
        int result = -1;
        if (!ar.equals(temp)) {

            try (Connection conn = getConnection();
                    PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
                ps.setString(1, ar.getTitle());
                ps.setString(2, ar.getLocation());
                ps.setInt(3, ar.getWholeDay());
                ps.setTimestamp(4, ar.getStartTime());
                ps.setTimestamp(5, ar.getEndTime());
                ps.setString(6, ar.getDetails());
                ps.setInt(7, ar.getAppointmentGroup());
                ps.setInt(8, ar.getAlarmReminderRequested());
                ps.setInt(9, ar.getID());
                try {
                    result = ps.executeUpdate();
                } catch (SQLException ex) {
                    log.debug("Exception: ", ex);
                    throw ex;
                }
                log.debug("end update(AppointmentRecord ar)");
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }
        }
        return result;
    }

    @Override
    public int update(AppointmentGroupRecord agr) throws SQLException {
        log.debug("start update(AppointmentGroupRecord agr)" + agr.toString());
        AppointmentGroupRecord temp = findAppointmentGroupRecordById(agr.getID());
        String query = "UPDATE APPOINTMENTGROUPRECORD SET GROUPNAME= ?, COLOR= ? WHERE ID = ?";
        int result = -1;
        if (!agr.equals(temp)) {
            try (Connection conn = getConnection();
                    PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
                ps.setString(1, agr.getGroupName());
                ps.setString(2, agr.getColor());
                ps.setInt(3, agr.getID());

                try {
                    result = ps.executeUpdate();
                } catch (SQLException ex) {
                    log.debug("Exception: ", ex);
                    throw ex;
                }
                log.debug("end update(AppointmentGroupRecord agr)");
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }
        }
        return result;
    }

    @Override
    public int update(Smtp smtp) throws SQLException {
        log.debug("start update(Smtp smtp)" + smtp.toString());
        Smtp temp = findSmtpById(smtp.getID());
        String query = "UPDATE SMTP SET username= ?, email= ?, password= ?, url= ?, portnumber= ?, isdefault= ?, reminderinterval = ? where ID = ?";
        int result = -1;
        if (!temp.equals(smtp)) {

            try (Connection conn = getConnection();
                    PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
                ps.setString(1, smtp.getUserName());
                ps.setString(2, smtp.getEmail());
                ps.setString(3, smtp.getPassword());
                ps.setString(4, smtp.getUrl());
                ps.setInt(5, smtp.getPortNumber());
                ps.setInt(6, smtp.getIsDefault());
                ps.setInt(7, smtp.getReminderInterval());
                ps.setInt(8, smtp.getID());
                try {
                    result = ps.executeUpdate();
                    log.debug("executeUpdate result code " + result);
                } catch (SQLException ex) {
                    log.debug("Exception: ", ex);
                    throw ex;
                }
                log.debug("end update(Smtp smtp)");
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }
        }
        return result;
    }

    @Override
    public int deleteAppointment(int ID) throws SQLException {
        log.debug("start deleteAppointment(int ID)");
        String query = "DELETE FROM APPOINTMENTRECORD WHERE ID = ?";

        int result = -1;
        int i = 1;
        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            ps.setInt(i, ID);
            try {
                result = ps.executeUpdate();
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end deleteAppointment(int ID)");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return result;
    }

    @Override
    public int deleteAppointmentGroup(int ID) throws SQLException {
        log.debug("start deleteAppointmentGroup(int ID)");
        String query = "DELETE FROM APPOINTMENTGROUPRECORD WHERE ID = ?";
        int result = -1;

        int i = 1;
        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            ps.setInt(i, ID);
            try {
                result = ps.executeUpdate();
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("start deleteAppointmentGroup(int ID)");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return result;
    }

    @Override
    public int deleteSmtp(int ID) throws SQLException {
        log.debug("start deleteSmtp(int ID)");
        String query = "DELETE FROM SMTP WHERE ID = ?";
        int result = -1;

        int i = 1;
        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            ps.setInt(i, ID);
            try {
                result = ps.executeUpdate();
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end  deleteSmtp(int ID)");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return result;
    }

    private AppointmentRecord makeAppointmentRecord(ResultSet rs) throws SQLException {
        log.debug("start makeAppointmentRecord(ResultSet rs)");
        AppointmentRecord record = new AppointmentRecord();
        record.setID(rs.getInt("ID"));
        record.setTitle(rs.getString("TITLE"));
        record.setLocation(rs.getString("LOCATION"));
        record.setWholeDay(rs.getInt("WHOLEDAY"));
        record.setStartTimeStamp(rs.getTimestamp("STARTTIME"));
        record.setEndTimeStamp(rs.getTimestamp("ENDTIME"));
        record.setDetails(rs.getString("DETAILS"));
        record.setAppointmentGroup(rs.getInt("APPOINTMENTGROUP"));
        record.setAlarmReminderRequested(rs.getInt("ALARMREMINDERREQUESTED"));
        log.debug("end makeAppointmentRecord(ResultSet rs)");
        return record;
    }

    private AppointmentGroupRecord makeAppointmentGroupRecord(ResultSet resultSet) throws SQLException {
        log.debug("start makeAppointmentGroupRecord(ResultSet resultSet) ");
        AppointmentGroupRecord agr = new AppointmentGroupRecord();
        agr.setID(resultSet.getInt("ID"));
        agr.setGroupName(resultSet.getString("GROUPNAME"));
        agr.setColor(resultSet.getString("COLOR"));
        log.debug("end makeAppointmentGroupRecord(ResultSet resultSet) ");
        return agr;
    }

    private Smtp makeSmptRecord(ResultSet rs) throws SQLException {
        log.debug("start makeSmptRecord(ResultSet rs) ");
        Smtp s = new Smtp();
        s.setID(rs.getInt("ID"));
        s.setUserName(rs.getString("USERNAME"));
        s.setEmail(rs.getString("EMAIL"));
        s.setPassword(rs.getString("PASSWORD"));
        s.setUrl(rs.getString("URL"));
        s.setPortNumber(rs.getInt("PORTNUMBER"));
        s.setIsDefault(rs.getInt("ISDEFAULT"));
        s.setReminderInterval(rs.getInt("REMINDERINTERVAL"));
        log.debug("end makeSmptRecord(ResultSet rs) ");
        return s;
    }

    public Smtp findDefaultSmtp() throws SQLException {
        log.debug("start findDefaultSmtp");
        String query = "SELECT * FROM SMTP WHERE ISDEFAULT = 1";
        Smtp smtp = new Smtp();

        try (Connection conn = getConnection();
                PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);) {
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    smtp = makeSmptRecord(resultSet);
                }
            } catch (SQLException ex) {
                log.debug("Exception: ", ex);
                throw ex;
            }

            log.debug("end findDefaultSmtp");
        } catch (SQLException ex) {
            log.debug("Exception: ", ex);
            throw ex;
        }
        return smtp;
    }

    private Connection getConnection() throws SQLException {
        log.debug("start build connection");
        Connection connection = DriverManager
                .getConnection(credentials.getUrl(), credentials.getUser(), credentials.getPassword());
        log.debug("end getConnection");
        return connection;

    }

    private AppointmentRecord createBoundAppointmentRecord(ResultSet rs, AppointmentRecord record) throws SQLException {
        log.debug("start createBoundAppointmentRecord(ResultSet rs, with appointmentRecord)" + record.toString());

        record.setID(rs.getInt("ID"));
        log.debug(rs.getInt("ID") + "");
        record.setTitle(rs.getString("TITLE"));
        record.setLocation(rs.getString("LOCATION"));
        record.setWholeDay(rs.getInt("WHOLEDAY"));
        record.setStartTimeStamp(rs.getTimestamp("STARTTIME"));
        record.setEndTimeStamp(rs.getTimestamp("ENDTIME"));
        record.setDetails(rs.getString("DETAILS"));
        record.setAppointmentGroup(rs.getInt("APPOINTMENTGROUP"));
        record.setAlarmReminderRequested(rs.getInt("ALARMREMINDERREQUESTED"));
        log.debug("end createBoundAppointmentRecord(ResultSet rs)" + record.toString());

        return record;
    }

    private AppointmentGroupRecord createBoundAppointmentGroupRecord(ResultSet rs, AppointmentGroupRecord record) throws SQLException {
        log.debug("start createBoundAppointmentGroupRecord(ResultSet rs, with AppointmentGroupRecord)" + record.toString());
        record.setID(rs.getInt("ID"));
        record.setGroupName(rs.getString("GROUPNAME"));
        record.setColor(rs.getString("COLOR"));
        log.debug("end createBoundAppointmentGroupRecord(ResultSet rs, with AppointmentGroupRecord)" + record.toString());
        return record;
    }

    private Smtp createBoundSmtpRecord(ResultSet rs, Smtp record) throws SQLException {
        log.debug("startcreateBoundSmtpRecord" + record.toString());
        record.setID(rs.getInt("ID"));
        record.setUserName(rs.getString("USERNAME"));
        record.setEmail(rs.getString("EMAIL"));
        record.setPassword(rs.getString("PASSWORD"));
        record.setUrl(rs.getString("URL"));
        record.setPortNumber(rs.getInt("PORTNUMBER"));
        record.setIsDefault(rs.getInt("ISDEFAULT"));
        record.setReminderInterval(rs.getInt("REMINDERINTERVAL"));
        return record;
    }

}
