package com.ericbehughes.database;

import com.ericbehughes.models.AppointmentGroupRecord;
import com.ericbehughes.models.AppointmentRecord;
import com.ericbehughes.models.Smtp;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

/**
 * Interface for CRUD methods
 *
 * @author eric hughes
 * @version 2.0
 */
public interface IJamDAO {

    // Create
    public int create(AppointmentRecord appointment) throws SQLException;

    public int create(AppointmentGroupRecord agr) throws SQLException;

    public int create(Smtp s) throws SQLException;

    // Read
    //find by id
    public AppointmentRecord findAppointmentByID(int id) throws SQLException;

    public AppointmentGroupRecord findAppointmentGroupRecordById(int id) throws SQLException;

    //find by title
    public AppointmentRecord findAppointmentByTitle(String title) throws SQLException;

    //find by group name
    public AppointmentGroupRecord findAppointmentGroupByName(String name) throws SQLException;

    // find allRecords by date and time
    public List<AppointmentRecord> findAllAppointmentsByDateAndTime(Timestamp t) throws SQLException;

    public List<AppointmentRecord> findAllAppointmentsForGivenWeek(Timestamp t) throws SQLException;

    public List<AppointmentRecord> findAllAppointmentsForGivenMonth(Timestamp t) throws SQLException;

    public List<AppointmentRecord> findAllAppointmentsBetween(Timestamp t1, Timestamp t2) throws SQLException;

    public List<AppointmentRecord> findAllAppointments() throws SQLException;

    // find all AppointmentGroups
    public List<AppointmentGroupRecord> findAllGroups() throws SQLException;

    //find all Smtp records
    public List<Smtp> findAllSmtpConfigurations() throws SQLException;

    // find all Appointments between 2 dates   
    // find smtp by id
    public Smtp findSmtpById(int id) throws SQLException;

    // Update
    public int update(AppointmentRecord ar) throws SQLException;

    public int update(AppointmentGroupRecord agr) throws SQLException;

    public int update(Smtp smtp) throws SQLException;

    // Delete
    public int deleteAppointment(int ID) throws SQLException;

    public int deleteAppointmentGroup(int ID) throws SQLException;

    public int deleteSmtp(int ID) throws SQLException;
}
