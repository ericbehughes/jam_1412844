/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ericbehughes.database;

import com.ericbehughes.database.JamDAO;
import com.ericbehughes.models.DatabaseCredentials;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import static java.nio.file.Files.newInputStream;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ehugh
 */
public class JamDbUtil {

    private final Logger log = LoggerFactory.getLogger(
            this.getClass().getName());
    private JamDAO jamObj;
    private final DatabaseCredentials credentials;

    //build db connection and pass to DAO object
    //give JAMDAO db connection through its constructor
    public JamDbUtil() {
          log.debug("inside DBUtility constructor");
        this.credentials = setDatabaseCredentials();
        log.debug("credentials " + credentials.getUrl() + "  " + credentials.getUser() + " " + credentials.getPassword());
        log.debug("inside finish setDatabaseCredentials");
        log.debug("begin JAMDAO object inside DBUtility");
        jamObj = new JamDAO(this.credentials); 
        log.debug("end JAMDAO object inside DBUtility");
        log.debug("finish seeding database inside DBUtility constructor");
    }

    public DatabaseCredentials getCredentials() {
        return credentials;
    }

   public void changeCredentials(DatabaseCredentials credentials){
       this.jamObj = new JamDAO(credentials);
   }
    private DatabaseCredentials setDatabaseCredentials() {
        Properties prop = new Properties();

        Path txtFile = get(".", "DatabaseCredentials.properties");
        // File must exist
        if (Files.exists(txtFile)) {
            try (InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(JamDbUtil.class.getName()).log(Level.SEVERE, null, ex);
            }

            String user = prop.getProperty("user");
            String password = prop.getProperty("password");
            String url = prop.getProperty("url");
            return new DatabaseCredentials(user, password, url);
        }
        return null;
    }

   
    
    
    public JamDAO getJamDAO() {
        return this.jamObj;
    }
}
