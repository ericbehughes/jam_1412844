/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ericbehughes.models;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import org.slf4j.LoggerFactory;

public class AppointmentRecord {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass().getName());

    private SimpleIntegerProperty ID;
    private SimpleStringProperty title;
    private SimpleStringProperty location;
    private SimpleObjectProperty<Timestamp> startTimeStamp;
    private SimpleObjectProperty<LocalDate> startDate;
    private SimpleStringProperty startTime;
    private SimpleObjectProperty<Timestamp> endTimeStamp;
    private SimpleObjectProperty<LocalDate> endDate;
    private SimpleStringProperty endTime;
    private SimpleStringProperty details;
    private SimpleIntegerProperty wholeDay;
    private SimpleBooleanProperty wholeDayBoolean;
    private SimpleIntegerProperty appointmentGroup;
    private SimpleIntegerProperty alarmReminderRequested;
    private SimpleBooleanProperty alarmReminderRequestedBoolean;

    private DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH:mm");

    public AppointmentRecord() {
        this(-1, "", "", Timestamp.valueOf(LocalDateTime.MIN), Timestamp.valueOf(LocalDateTime.MIN), "", 0, 0, 0);
    }

    public AppointmentRecord(int ID, String title, String location, Timestamp startTime, Timestamp endTime, String details, int wholeDay, int appointmentGroup, int alarmReminderRequested) {
        this.ID = new SimpleIntegerProperty(ID);
        this.title = new SimpleStringProperty(title);
        this.location = new SimpleStringProperty(location);

        this.startTimeStamp = new SimpleObjectProperty<Timestamp>(startTime);
        this.startDate = new SimpleObjectProperty<LocalDate>(startTime.toLocalDateTime().toLocalDate());
        //log.debug("this.startDate" + startTime);
        this.startTime = new SimpleStringProperty(endTime.toLocalDateTime().toLocalTime().format(hourFormatter));

        this.endTimeStamp = new SimpleObjectProperty<Timestamp>(endTime);
        this.endDate = new SimpleObjectProperty<LocalDate>(startTime.toLocalDateTime().toLocalDate());
        this.endTime = new SimpleStringProperty(endTime.toLocalDateTime().toLocalTime().toString());

        this.details = new SimpleStringProperty(details);
        this.wholeDay = new SimpleIntegerProperty(wholeDay);
        this.wholeDayBoolean = new SimpleBooleanProperty((wholeDay == 1));
        this.appointmentGroup = new SimpleIntegerProperty(appointmentGroup);
        this.alarmReminderRequested = new SimpleIntegerProperty(alarmReminderRequested);
        this.alarmReminderRequestedBoolean = new SimpleBooleanProperty((alarmReminderRequested == 1));
    }

    public SimpleIntegerProperty getIDProperty() {
        return ID;
    }

    public void setID(int ID) {
        this.ID.set(ID);
    }

    public SimpleStringProperty getTitleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public SimpleStringProperty getLocationProperty() {
        return location;
    }

    public void setLocation(String location) {
        this.location.set(location);
    }

    public SimpleObjectProperty<Timestamp> getStartTimeStampProperty() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(Timestamp startTimeStamp) {

        this.startTimeStamp.set(startTimeStamp);
        this.startDate.set(startTimeStamp.toLocalDateTime().toLocalDate());
        this.startTime.set(startTimeStamp.toLocalDateTime().format(hourFormatter));

    }

    public SimpleObjectProperty<LocalDate> getStartDateProperty() {
        return startDate;
    }

    public SimpleStringProperty getStartTimeProperty() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime.set(startTime);
    }

    public SimpleObjectProperty<Timestamp> getEndTimeStampProperty() {
        return endTimeStamp;
    }

    public void setEndTimeStamp(Timestamp endTimeStamp) {
        this.endTimeStamp.set(endTimeStamp);
        this.endDate.set(endTimeStamp.toLocalDateTime().toLocalDate());
        this.endTime.set(endTimeStamp.toLocalDateTime().format(hourFormatter));
    }

    public SimpleObjectProperty<LocalDate> getEndDateProperty() {
        return endDate;
    }

    public SimpleStringProperty getEndTimeProperty() {
        return endTime;
    }

    public SimpleStringProperty getDetailsProperty() {
        return details;
    }

    public void setDetails(String details) {
        this.details.set(details);
    }

    public SimpleIntegerProperty getWholeDayProperty() {
        return wholeDay;
    }

    public SimpleBooleanProperty getWholeDayBooleanPropertyProperty() {
        return wholeDayBoolean;
    }

    public void setWholeDay(Integer wholeDay) {
        this.wholeDay.set(wholeDay);
        this.wholeDayBoolean.set((wholeDay == 1));
        if (wholeDay == 1) {
            this.startTime.set(LocalDateTime.now().MIN.format(hourFormatter));
            this.endTime.set(LocalDateTime.now().MAX.format(hourFormatter));
        }

    }

    public SimpleIntegerProperty getAppointmentGroupProperty() {
        return appointmentGroup;
    }

    public void setAppointmentGroup(int appointmentGroup) {
        this.appointmentGroup.set(appointmentGroup);
    }

    public SimpleIntegerProperty getAlarmReminderRequestedProperty() {
        return alarmReminderRequested;
    }

    public SimpleBooleanProperty getAlarmReminderRequestedBooleanProperty() {
        return alarmReminderRequestedBoolean;
    }

    public void setAlarmReminderRequested(int alarmReminderRequested) {
        this.alarmReminderRequested.set(alarmReminderRequested);
        this.alarmReminderRequestedBoolean.set((alarmReminderRequested == 1));
    }

    public int getID() {
        return ID.get();
    }

    public String getTitle() {
        return title.get();
    }

    public String getLocation() {
        return location.get();
    }

    public Timestamp getStartTime() {
        LocalDateTime l = LocalDateTime.of(startDate.get(), LocalTime.parse(startTime.get()));
        Timestamp t = Timestamp.valueOf(l);
        return t;
    }

    public Timestamp getEndTime() {
        LocalDateTime l = LocalDateTime.of(endDate.get(), LocalTime.parse(endTime.get()));
        Timestamp t = Timestamp.valueOf(l);
        return t;
    }

    public String getDetails() {
        return details.get();
    }

    public int getWholeDay() {
        return wholeDayBoolean.get() ? 1 : 0;
    }

    public int getAppointmentGroup() {
        return appointmentGroup.get();
    }

    public int getAlarmReminderRequested() {
        return alarmReminderRequestedBoolean.get() ? 1 : 0;
    }

    @Override
    public String toString() {
        return "ID=" + ID.get() + title.get() + ", \nlocation=" + location.get() + "\n" + startDate.get() + startTime.get() + "   " + startTimeStamp.toString()
                + "\n" + endDate.get() + endTime.get() + "\n" + details.get() + "\nappointmentGroup=" + appointmentGroup.get();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.ID);
        hash = 83 * hash + Objects.hashCode(this.title);
        hash = 83 * hash + Objects.hashCode(this.location);
        hash = 83 * hash + Objects.hashCode(this.startTimeStamp);
        hash = 83 * hash + Objects.hashCode(this.startDate);
        hash = 83 * hash + Objects.hashCode(this.startTime);
        hash = 83 * hash + Objects.hashCode(this.endTimeStamp);
        hash = 83 * hash + Objects.hashCode(this.endDate);
        hash = 83 * hash + Objects.hashCode(this.endTime);
        hash = 83 * hash + Objects.hashCode(this.details);
        hash = 83 * hash + Objects.hashCode(this.wholeDay);
        hash = 83 * hash + Objects.hashCode(this.appointmentGroup);
        hash = 83 * hash + Objects.hashCode(this.alarmReminderRequested);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AppointmentRecord other = (AppointmentRecord) obj;
        if (!Objects.equals(this.ID, other.ID)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.location, other.location)) {
            return false;
        }
        if (!Objects.equals(this.startTimeStamp, other.startTimeStamp)) {
            return false;
        }
        if (!Objects.equals(this.startDate, other.startDate)) {
            return false;
        }
        if (!Objects.equals(this.startTime, other.startTime)) {
            return false;
        }
        if (!Objects.equals(this.endTimeStamp, other.endTimeStamp)) {
            return false;
        }
        if (!Objects.equals(this.endDate, other.endDate)) {
            return false;
        }
        if (!Objects.equals(this.endTime, other.endTime)) {
            return false;
        }
        if (!Objects.equals(this.details, other.details)) {
            return false;
        }
        if (!Objects.equals(this.wholeDay, other.wholeDay)) {
            return false;
        }
        if (!Objects.equals(this.appointmentGroup, other.appointmentGroup)) {
            return false;
        }
        if (!Objects.equals(this.alarmReminderRequested, other.alarmReminderRequested)) {
            return false;
        }
        return true;
    }

}
