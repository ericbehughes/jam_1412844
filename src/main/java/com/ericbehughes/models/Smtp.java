/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ericbehughes.models;

import java.util.Objects;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ehugh
 */
public class Smtp {

    private SimpleIntegerProperty ID;
    private SimpleStringProperty userName;
    private SimpleStringProperty email;
    private SimpleStringProperty password;
    private SimpleStringProperty url;
    private SimpleIntegerProperty portNumber;
    private SimpleIntegerProperty isDefault;
    private SimpleBooleanProperty isDefaultBoolean;
    private SimpleIntegerProperty reminderInterval;
       private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass().getName());
 
    public Smtp() {
        this(-1, "", "", "", "", -1, -1, -1);
    }

    public Smtp(int ID, String userName, String email, String password, String url, int portNumber, int isDefault, int reminderInterval) {
        this.ID = new SimpleIntegerProperty(ID);
        this.userName = new SimpleStringProperty(userName);
        this.email = new SimpleStringProperty(email);
        this.password = new SimpleStringProperty(password);
        this.url = new SimpleStringProperty(url);
        this.portNumber = new SimpleIntegerProperty(portNumber);
        this.isDefault = new SimpleIntegerProperty(isDefault);
        this.isDefaultBoolean = new SimpleBooleanProperty((isDefault == 1));
        this.reminderInterval = new SimpleIntegerProperty(reminderInterval);
    }

    public int getID() {
        return ID.get();
    }

    public SimpleIntegerProperty getIDProperty() {
        return ID;
    }

    public void setID(int ID) {
        this.ID.set(ID);
    }

    public String getUserName() {
        return userName.get();
    }

    public SimpleStringProperty getUserNameProperty() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName.set(userName);
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty getEmailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getPassword() {
        return password.get();
    }

    public SimpleStringProperty getPasswordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public String getUrl() {
        return url.get();
    }

    public SimpleStringProperty getUrlProperty() {
        return url;
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public int getPortNumber() {
        return portNumber.get();
    }

    public SimpleIntegerProperty getPortNumberProperty() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber.set(portNumber);
    }

    public int getIsDefault() {
        return isDefaultBoolean.get() ? 1 : 0;
    }

    public SimpleIntegerProperty getIsDefaultProperty() {
        return isDefault;
    }
    
    public SimpleBooleanProperty getIsDefaultBooleanProperty() {
        return isDefaultBoolean;
    }

    public void setIsDefault(int isDefault) {
        log.debug("inside setIsDefault " + isDefault);
        this.isDefault.set(isDefault);
        this.isDefaultBoolean.set( (isDefault == 1));

    }

    public int getReminderInterval() {
        return reminderInterval.get();
    }
    
     public SimpleIntegerProperty getReminderIntervalProperty() {
        return reminderInterval;
    }

    public void setReminderInterval(int reminderInterval) {
        this.reminderInterval.set(reminderInterval);
    }

    @Override
    public String toString() {
        return "Smtp{" + "ID=" + ID + ", userName=" + userName + ", email=" + email + ", password=" + password + ", url=" + url + ", portNumber=" + portNumber + ", isDefault=" + isDefault + ", isDefaultBoolean=" + isDefaultBoolean + ", reminderInterval=" + reminderInterval + '}';
    }
    
    
    

    


   

}
