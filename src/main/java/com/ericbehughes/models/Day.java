/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ericbehughes.models;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ehugh
 */
public class Day {

    private final Logger log = LoggerFactory.getLogger(getClass().getName());
    private SimpleObjectProperty<LocalDateTime> time;
    private List<AppointmentRecord> appointments;

    public Day(Day day){
        this.time = day.getLocalDateTimeProperty();
        this.appointments = day.getAppointmentsList();
        this.time = day.getLocalDateTimeProperty();
    }
    public Day(LocalDateTime t, List<AppointmentRecord> appointments) {

        this.time = new SimpleObjectProperty<LocalDateTime>(t);
        //localDateTime.plusMinutes(30);
        this.appointments = appointments;
    }

    public void setTime(LocalDateTime time) {
        this.time.set(time);
    }
    
    public List<AppointmentRecord> getAppointmentsList(){
        return this.appointments;
    }

    public LocalDateTime getLocalDateTime() {
        return this.time.get();
    }

    public LocalDate getLocalDate() {
        return this.time.get().toLocalDate();
    }

    public void setLocalDate(LocalDateTime ld) {
        this.time.set(ld);
    }

    public SimpleObjectProperty<LocalDateTime> getLocalDateTimeProperty() {
        return this.time;
    }

    //  this(-1, "", "", Timestamp.valueOf(LocalDateTime.now()),Timestamp.valueOf(LocalDateTime.now()) , "", 0,0,0);
    public String getAppointments() {
        
        log.debug("getAppointments size for current day" + appointments.size());
        log.debug("getAppointments this.time" + this.time);
        String s = "";
        if (appointments.size() > 0) {
            for (AppointmentRecord appointment : appointments) {
                s += appointment.getTitle().trim() + "\n" + appointment.getStartTime().toLocalDateTime()
                        + "\n" + appointment.getEndTime().toLocalDateTime() + "\n\n";

            }
        }

        return s.trim();

    }

    /**
     * getAppointments for time appends the appointments to the table view cells
     * it checks the times and appends them to a string in order of their date
     * @return 
     */
    public SimpleStringProperty getAppointsmentsForTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        log.debug("return appointments size for current day" + appointments.size());
        String s = "";


        if (appointments.size() > 0) {
            log.debug("inside getAppointmentsForTime" + appointments.get(0));
            for (AppointmentRecord a : appointments) {
                LocalTime apptStartTime = a.getStartTime().toLocalDateTime().toLocalTime();
                LocalTime apptEndTime = a.getEndTime().toLocalDateTime().toLocalTime();
                LocalTime colTime = this.time.getValue().toLocalTime();
                 // check if whole day 
                if (a.getWholeDay() == 1){
                      s+= a.getTitle().trim() + "\n" + a.getStartTime().toLocalDateTime().format(formatter) + " " + a.getEndTime().toLocalDateTime().format(formatter) + "\n\n";
                      return new SimpleStringProperty(s);
                }
                      
                
                 log.debug(a.getStartTime().toLocalDateTime().toLocalTime() + "is after this time  colTime" + colTime);
                 log.debug( "is after this time  colTime" + a.getStartTime().toLocalDateTime().toLocalTime().isAfter(colTime) + "");
                if (apptStartTime.isAfter(colTime)
                        && apptStartTime.isBefore(colTime.plusMinutes(29).plusSeconds(59))) {
                    log.debug(a.getStartTime() + "is after this time  colTime" + colTime);
                    s += a.getTitle().trim() + "\n" + a.getStartTime().toLocalDateTime().format(formatter) + " " + a.getEndTime().toLocalDateTime().format(formatter) + "\n\n";
                } else if (apptEndTime.isAfter(colTime)
                        && apptEndTime.isBefore(colTime.plusMinutes(29).plusSeconds(59))) {
                    s += a.getTitle().trim() + "\n" + a.getStartTime().toLocalDateTime().format(formatter) + " " + a.getEndTime().toLocalDateTime().format(formatter) + "\n\n";
                } else if (colTime.isBefore(apptStartTime)
                        && colTime.isAfter(apptEndTime)) {
                    s += a.getTitle().trim() + "\n" + a.getStartTime().toLocalDateTime() + " " + a.getEndTime().toLocalDateTime() + "\n\n";

                    //s += appointment.getStartTime().toLocalDateTime() + "\n\n" ;
                }
            }
        }

        return new SimpleStringProperty(s);

    }

}
