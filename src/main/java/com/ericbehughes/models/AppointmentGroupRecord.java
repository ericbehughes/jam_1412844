/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ericbehughes.models;

import java.util.Objects;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.paint.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ehugh
 */
public class AppointmentGroupRecord {

    private SimpleIntegerProperty ID;
    private SimpleStringProperty groupName;
    private SimpleStringProperty color;
    private SimpleObjectProperty<Color> colorProperty;
    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());

    // properties
    public AppointmentGroupRecord() {
        this(-1, "", "fff");
    }

    public AppointmentGroupRecord(int groupNumber, String groupName, String color) {
        this.ID = new SimpleIntegerProperty(groupNumber);
        this.groupName = new SimpleStringProperty(groupName);
        log.debug("inside appointmentgrouprecord constructor value colr  " + color + color.length());
        this.color = new SimpleStringProperty(color);
        this.colorProperty = new SimpleObjectProperty<Color>(Color.web("#" + color));
    }

    public int getID() {
        return this.ID.get();
    }

    public void setID(int groupNumber) {
        this.ID.set(groupNumber);
    }

    public SimpleIntegerProperty getIDProperty() {
        return this.ID;
    }

    public String getGroupName() {
        return groupName.get();
    }

    public void setGroupName(String groupName) {
        this.groupName.set(groupName);
    }

    public SimpleStringProperty getGroupNameProperty() {
        return this.groupName;
    }

    public String getColor() {
        return colorProperty.get().toString().substring(2, 8);
    }

    public void setColor(String color) {
        this.color.set(color);
        log.debug("inside setColor  " + color);
        this.colorProperty.set(Color.web("#" + color));
    }

    public SimpleObjectProperty<Color> getColorProperty() {
        return colorProperty;
    }

    public void setColorProperty(SimpleObjectProperty<Color> c) {
        colorProperty = c;
    }

    @Override
    public String toString() {
        return "AppointmentGroupRecord{" + "ID=" + ID + ", groupName=" + groupName + ", color=" + colorProperty + '}';

    }

}
