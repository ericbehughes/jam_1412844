/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ericbehughes.models;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author ehugh
 */
public class DatabaseCredentials {

    private SimpleStringProperty user;
    private SimpleStringProperty password;
    public DatabaseCredentials(){
        this("", "", "");
    }
    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public DatabaseCredentials(String user, String password, String url) {
        this.user = new SimpleStringProperty(user);
        this.password = new SimpleStringProperty(password);
        this.url = url;
    }


    public String getUser() {
        return user.get();
    }

    public SimpleStringProperty getUserProperty() {
        return user;
    }

    public void setUser(String user) {
        this.user.set(user);
    }

    public String getPassword() {
        return password.get();
    }
    
     public SimpleStringProperty getPasswordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }
}
