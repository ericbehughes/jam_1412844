/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ericbehughes.models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ehugh
 */
public class Week {
    private final Logger log = LoggerFactory.getLogger(getClass().getName());
    private SimpleObjectProperty<LocalDateTime> time;
    private SimpleObjectProperty<LocalDate> date;
    private List<SimpleObjectProperty<Day>> list = new ArrayList<SimpleObjectProperty<Day>>();
     private List<Day> listOfDays = new ArrayList<>();

    public Week(List<SimpleObjectProperty<Day>> dailyViews, LocalDateTime t) {
        this.list = new ArrayList(dailyViews);
        
        for (int j = 0; j < list.size(); j ++) {
            
            this.listOfDays.add(list.get(j).get());
        }
        this.date = new SimpleObjectProperty<LocalDate>(t.toLocalDate());
        this.time = new SimpleObjectProperty<LocalDateTime>(t);
        
    }
    
    
    public Day getWeek(int index) {
        return this.list.get(index).get();
    }
    
     public Day getListOfDays(int index) {
        return this.listOfDays.get(index);
    }
    
    public List<SimpleObjectProperty<Day>> getWeek(){
        return this.list;
    }
    
    public LocalDate getLocalDateForCurrentDay(int index){
        return this.list.get(index).getValue().getLocalDate();
    }
    
    public void setLocalDateForCurrentDay(int index, LocalDateTime ld){
        this.list.get(index).get().setLocalDate(ld);
    }

    public LocalDateTime getLocalDateTime() {
        return this.time.get();
    }
    
    public String getAppointment(int index){
        log.debug("weeks size" + this.list.size());
        log.debug(this.list.toString());
        return this.list.get(index).getValue().getAppointments();
    }
    
    public void setDays(List<SimpleObjectProperty<Day>> list){
        this.list = list;
    }

    public SimpleObjectProperty<LocalDateTime> getLocalDateTimeProperty() {
        return this.time;
    }
    
    public SimpleObjectProperty<LocalDate> getLocalDateProperty() {
        return this.date;
    }
    
      /**
     * getAppointsmentsForTime for time appends the appointments to the table view cells
     * it checks the times and appends them to a string in order of their date
     * @return 
     */
      public String getAppointsmentsForTime(int i ) {
           DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String s = "";
        // this is another change
        if (listOfDays.get(i).getAppointmentsList().size() > 0) {
            log.debug("inside getAppointmentsForTime" + list.get(i).get());
            for (AppointmentRecord a : listOfDays.get(i).getAppointmentsList()) {
               
                LocalTime apptStartTime = a.getStartTime().toLocalDateTime().toLocalTime();
                LocalTime apptEndTime = a.getEndTime().toLocalDateTime().toLocalTime();
                LocalTime colTime = this.time.getValue().toLocalTime();
                 if (a.getWholeDay() == 1){
                      s+= a.getTitle().trim() + "\n" + a.getStartTime().toLocalDateTime().format(formatter) + " " 
                              + a.getEndTime().toLocalDateTime().format(formatter) + "\n\n";
                      return  s;
                }
                 log.debug(a.getStartTime().toLocalDateTime().toLocalTime() + "is after this time  colTime" + colTime);
                 log.debug( "is after this time  colTime" + a.getStartTime().toLocalDateTime().toLocalTime().isAfter(colTime) + "");
                if (apptStartTime.isAfter(colTime)
                        && apptStartTime.isBefore(colTime.plusMinutes(29).plusSeconds(59))) {
                    log.debug(a.getStartTime() + "is after this time  colTime" + colTime);
                    s += a.getTitle().trim() + "\n" + a.getStartTime().toLocalDateTime().format(formatter) + 
                            "\n" + a.getEndTime().toLocalDateTime().format(formatter) + 
                            "\n\n";
                } else if (apptEndTime.isAfter(colTime)
                        && apptEndTime.isBefore(colTime.plusMinutes(29).plusSeconds(59))) {
                    s += a.getTitle().trim() + "\n" + a.getStartTime().toLocalDateTime().format(formatter) + 
                            "\n" + a.getEndTime().toLocalDateTime().format(formatter) + 
                            "\n\n";
                } else if (colTime.isBefore(apptStartTime)
                        && colTime.isAfter(apptEndTime)) {
                    s += a.getTitle().trim() + "\n" + a.getStartTime().toLocalDateTime().format(formatter) + 
                            "\n" + a.getEndTime().toLocalDateTime().format(formatter) + 
                            "\n\n";

                    //s += appointment.getStartTime().toLocalDateTime() + "\n\n" ;
                }
            }
        }
        return s;
      }

}
