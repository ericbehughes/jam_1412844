///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.ericbehughes.email;
//
//import com.ericbehughes.models.AppointmentRecord;
//import com.ericbehughes.models.Smtp;
//import java.io.File;
//import java.util.List;
//
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// *
// * @author ehugh
// */
//public class JAMEmail {
//    // Real programmers use logging
//    private final Logger log = LoggerFactory.getLogger(getClass().getName());
//
//    // These must be updated to your email accounts
//    private final String smtpServerName = "smtp.gmail.com";
//    private final String imapServerName = "imap.gmail.com";
//    private final String emailSend;
//    private final String emailSendPwd;
//    private final String emailReceive ;
//
//    // You will need a folder with this name or change it to another
//    // existing folder
//    private final String attachmentFolder = "C:\\Temp\\Attach\\";
//
//    public JAMEmail(Smtp s) {
//        log.debug("jamEmail constructor" + s.toString());
//        this.emailSend = s.getEmail();
//        this.emailSendPwd = s.getPassword();
//        this.emailReceive = s.getEmail();
//    }
//    
//    /**
//     * This method is where the different uses of Jodd are exercised
//     */
//    public void perform(AppointmentRecord ar) {
//        // Send an ordinary text message
//        log.info("inside JAMEmail before sendEmail(appointmentRecord)");
//        sendEmail(ar);
//        log.info("inside JAMEmail after sendEmail(appointmentRecord)");
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            log.error("Threaded sleep failed", e);
//            System.exit(1);
//        }
//    }
//
//    /**
//     * Standard send routine using Jodd. Jodd knows about GMail so no need to
//     * include port information
//     */
//    public void sendEmail(AppointmentRecord ar ){
//        
//        log.debug("inside sendEmail" + ar.toString());
//        // Create am SMTP server object
//        SmtpServer<SmtpSslServer> smtpServer = SmtpSslServer
//                .create(smtpServerName)
//                .authenticateWith(emailSend, emailSendPwd);
//
//        // Display Java Mail debug conversation with the server
//        smtpServer.debug(true);
//        
//        // Using the fluent style of coding create a plain text message
//        Email email = Email.create().from(emailSend)
//                .to(emailReceive)
//                .subject(ar.getTitle()).addText(ar.toString());
//
//        // A session is the object responsible for communicating with the server
//        SendMailSession session = smtpServer.createSession();
//
//        // Like a file we open the session, send the message and close the
//        // session
//        session.open();
//        session.sendMail(email);
//        session.close(); 
//    }
//   
//}
