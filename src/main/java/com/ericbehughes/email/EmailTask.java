///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.ericbehughes.email;
//
//import com.ericbehughes.database.JamDAO;
//import com.ericbehughes.database.JamDbUtil;
//import com.ericbehughes.models.AppointmentRecord;
//import com.ericbehughes.models.Smtp;
//import java.sql.SQLException;
//import java.sql.Timestamp;
//import java.time.LocalDateTime;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javafx.application.Platform;
//import javafx.scene.control.Alert;
//import org.slf4j.LoggerFactory;
//
///**
// *
// * @author ehugh
// */
//public class EmailTask implements Runnable {
//
//    private final org.slf4j.Logger log = LoggerFactory.getLogger(
//            this.getClass().getName());
//
//    // As we will be using this object in a thread it must be thread safe.
//    // This is accomplished by ensuring that all its class variables are final
//    // as well.
//
//    private final JamDbUtil dbUtil = new JamDbUtil();
//    private final JamDAO jamObj = dbUtil.getJamDAO();
//
//    /**
//     * Constructor creates an instance of the thread safe EmailSender
//     */
//    public EmailTask(Smtp s) {
//        log.debug("email task constructor");
//        emailSender = new JAMEmail(s);
//
//        log.debug("emailTask constructor after new JAMEmail(s)");
//    }
//
//    @Override
//    public void run() {
//
//        // Platform.runLater() allows this thread to interact with the main
//        // JavaFX thread and therefore interact with controls.
//        Platform.runLater(() -> {
//
//            try {
//                Timestamp t1 = Timestamp.valueOf(LocalDateTime.now().plusMinutes(117));
//                Timestamp t2 = Timestamp.valueOf(LocalDateTime.now().plusMinutes(123));
//                List<AppointmentRecord> appointmentsToSend = jamObj.findAllAppointmentsBetween(t1, t2);
//
//                if (appointmentsToSend.size() > 0) {
//                    for (AppointmentRecord appointmentRecord : appointmentsToSend) {
//                        //send email
//                        log.info("Sent an email");
//                        emailSender.perform(appointmentRecord);
//                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
//                        alert.setTitle("Sending An Email");
//                        alert.setHeaderText("Sending An Email.");
//                        alert.setContentText("An email has been sent.");
//                        alert.showAndWait();
//                    }
//                }
//
//            } catch (SQLException ex) {
//                Logger.getLogger(EmailTask.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        );
//
//    }
//}
