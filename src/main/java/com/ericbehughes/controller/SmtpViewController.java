package com.ericbehughes.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.ericbehughes.database.JamDAO;
import com.ericbehughes.models.Smtp;
import com.ericbehughes.models.Smtp;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Menu;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author ehugh
 */
public class SmtpViewController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private Smtp smtpModel;
    // JAM DAO object
    private JamDAO jamDAO;
    @FXML
    private AnchorPane smtpView;


    @FXML
    private Button smtpSaveBtn;

    @FXML
    private Button smtpNextBtn;

    @FXML
    private Button smtpPreviousBtn;

    @FXML
    private TextField smtpViewId;

    @FXML
    private TextField smtpUsernameViewId;

    @FXML
    private TextField smtpEmailViewId;

    @FXML
    private TextField smtpPasswordViewId;

    @FXML
    private TextField smtpUrlViewId;

    @FXML
    private TextField smtpPortNumberViewId;

    @FXML
    private CheckBox smtpIsDefaultViewId;

    @FXML
    private TextField smtpReminderIntervalViewId;
    
    private Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    

    private void doBindings() {
        log.debug("do bindings smtp tostring " + smtpModel.toString());
        Bindings.bindBidirectional(smtpViewId.textProperty(), smtpModel.getIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(smtpUsernameViewId.textProperty(), smtpModel.getUserNameProperty());
        Bindings.bindBidirectional(smtpEmailViewId.textProperty(), smtpModel.getEmailProperty());
        Bindings.bindBidirectional(smtpPasswordViewId.textProperty(), smtpModel.getPasswordProperty());
        Bindings.bindBidirectional(smtpUrlViewId.textProperty(), smtpModel.getUrlProperty());
        Bindings.bindBidirectional(smtpPortNumberViewId.textProperty(), smtpModel.getPortNumberProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(smtpIsDefaultViewId.selectedProperty(), smtpModel.getIsDefaultBooleanProperty());
        Bindings.bindBidirectional(smtpReminderIntervalViewId.textProperty(), smtpModel.getReminderIntervalProperty(), new NumberStringConverter());
    }
    
    @FXML
    private void onSmtpPrevious(Event e) {
        log.debug("com.ericbehughes.controller.SmtpViewController.onSmtpPrevious");
            try {
                jamDAO.findPrevByID(this.smtpModel);
                log.debug("end of findNextByID " + smtpModel.toString());
            } catch (Exception ex) {
                log.error("SQL Error", ex);
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            
        }

    }

    @FXML
    private void onSmtpNext(Event e) {
        log.debug("com.ericbehughes.controller.SmtpViewController.onSmtpNext");
            try {
                jamDAO.findNextByID(this.smtpModel);
                log.debug("end of findNextByID " + smtpModel.toString());
            } catch (Exception ex) {
                log.error("SQL Error", ex);
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    @FXML
    private void onSmtpSave(Event e) {
        log.debug("com.ericbehughes.controller.SmtpViewController.onSmtpSave");

        
            try {
                Smtp temp = jamDAO.findSmtpById(this.smtpModel.getID());
                if (temp.getID() == -1) {
                    log.debug("start onSmtpSave create" + this.smtpModel);
                    jamDAO.create(this.smtpModel);
                    log.debug("end of onSmtpSave create " + smtpModel.toString());
                } else {
                    log.debug("start onSmtpSave update" + this.smtpModel);
                    jamDAO.update(this.smtpModel);
                    log.debug("end onSmtpSave update" + smtpModel.toString());
                }

            } catch (Exception ex) {
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        

    }

    @FXML
    public void onSmtpClear() {
        log.debug("com.ericbehughes.controller.SmtpViewController.onSmtpClear");
        this.setClickedSmtpRecord(new Smtp());
    }

    @FXML
    public void onSmtpDelete() {
        log.debug("com.ericbehughes.controller.SmtpViewController.onSmtpDelete");
        alert.setTitle("Warning");
        alert.setHeaderText("Deleting Smtp");
        alert.setContentText("Once deleted you cannot retrieve this smtp record");
        alert.showAndWait();

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            try {
                jamDAO.deleteSmtp(this.smtpModel.getID());
                log.debug("end of deleteSmtp " + smtpModel.toString());
                onSmtpNext(null);
            } catch (Exception ex) {
                log.error("SQL Error", ex);
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            // ... user chose CANCEL or closed the dialog
        }
        onSmtpClear();
    }

    public void setJamDAO(Smtp smtp, JamDAO jamDAO) throws SQLException {
        this.smtpModel = smtp;
        log.debug("smtp controller start setJamDAO " + smtp.toString());
        this.jamDAO = jamDAO;
        log.debug("smtp inside setJamDAO jamDAO print" + jamDAO);
        try {
            jamDAO.findNextByID(smtp);
            log.debug("inside find nextByID  of smtp controller " + smtpModel.toString());
        } catch (SQLException ex) {
            Logger.getLogger(SmtpViewController.class.getName()).log(Level.SEVERE, null, ex);
        }

        doBindings();
    }
    private void setClickedSmtpRecord(Smtp s) {
        this.smtpModel.setID(s.getID());
        this.smtpModel.setUserName(s.getUserName());
        this.smtpModel.setEmail(s.getEmail());
        this.smtpModel.setPassword(s.getPassword());
        this.smtpModel.setUrl(s.getUrl());
        this.smtpModel.setPortNumber(s.getPortNumber());
        this.smtpModel.setIsDefault(s.getIsDefault());
        this.smtpModel.setReminderInterval(s.getReminderInterval());
        
    }
}
