package com.ericbehughes.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.ericbehughes.jam_1412844_1.MainApp;
import com.ericbehughes.models.AppointmentRecord;
import com.ericbehughes.models.Day;
import com.ericbehughes.models.Week;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author ehugh
 */
public class CalendarViewController implements Initializable {

    // Real programmers use logging
    private final Logger log = LoggerFactory.getLogger(getClass().getName());
    /**
     * Initializes the controller class.
     */
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd");
    //private final DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH:mm");

    /*
    reference to the main app passed by in MainApp
     */
    private MainApp app;

    // 3 table views for daily, weekly and monthly
    @FXML
    TableView<Day> dailyView = new TableView<>();

    @FXML
    TableView<Week> weeklyView = new TableView<>();

    @FXML
    TableView<Week> monthlyView = new TableView<>();

    // time columns for daily and weekly 
    @FXML
    TableColumn<Day, String> timeCol = new TableColumn<Day, String>("Time");
    @FXML
    TableColumn<Week, String> weeklyTimeCol = new TableColumn<Week, String>("Time");

    // columns for daily, weekly and monthly
    @FXML
    TableColumn<Day, String> dailyColumns = new TableColumn<Day, String>("Today's Appointments");
    @FXML
    List<TableColumn<Week, String>> weeklyColumns = new ArrayList<TableColumn<Week, String>>();
    @FXML
    List<TableColumn<Week, String>> monthlyColumns = new ArrayList<TableColumn<Week, String>>();

    /*
    cells for the click listener for the monthly and weekly view 
     */
    private ObservableList<TablePosition> monthlyCells;
    private ObservableList<TablePosition> weeklyCells;
    /*
    
     */
    private Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

    public CalendarViewController() {
        super();

    }
    /*
    labels for the weekly columns could be a resource bundle not enough time to implement
     */
    private final String[] weekDayLabels = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }


    /*
    the createDaily, createWeekly and createMonthly are all called in MainApp.java
    each begin by invoking their repopulateTable methods underneath the create methods
    
    after repopulateTable is returned the cellFactory is set which concatenates the appointments
    as a string in the daily and weekly bean classes
     */
    public TableView<Day> createDailyView(LocalDate ld) {
        repopulateDailyTable(LocalDateTime.of(ld, LocalTime.MIN));
        dailyColumns.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Day, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Day, String> foo) {
                log.debug("inside createDaily setCellFactory " + foo.getValue().getAppointments());
                log.debug("inside createDaily setCellFactory " + foo.getValue().getAppointsmentsForTime());
                return foo.getValue().getAppointsmentsForTime();
            }

        });

        renderCell(timeCol);
        renderCell(dailyColumns);
        timeCol.setPrefWidth(150);
        dailyColumns.setPrefWidth(700);
        dailyView.getSelectionModel().setCellSelectionEnabled(true);
        dailyView.getColumns().add(timeCol);
        dailyView.getColumns().add(dailyColumns);
        dailyView.setPrefWidth(800);
        dailyView.setFixedCellSize(120);

        return dailyView;

    }

    public TableView<Week> createWeeklyView(LocalDate ld) {

        repopulateWeeklyTable(LocalDateTime.of(ld, LocalTime.MIN));
        for (int i = 0; i < weeklyColumns.size(); i++) {
            weeklyColumns.get(i).setPrefWidth(150);
            final int counter = i;
            weeklyColumns.get(i).setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getAppointsmentsForTime(counter)));
            renderCell(weeklyColumns.get(i));
        }
        renderCell(weeklyTimeCol);
        weeklyTimeCol.setPrefWidth(100);
        weeklyView.setFixedCellSize(180);
        weeklyView.getColumns().add(weeklyTimeCol);
        weeklyView.getColumns().addAll(weeklyColumns);
        weeklyView.setPrefWidth(800);
        weeklyView.getSelectionModel().setCellSelectionEnabled(true);
        monthlyCells.addListener(this::showSingleCellDetails);
        return weeklyView;

    }

    public TableView<Week> createMonthlyView(LocalDate ld) {
        for (String name : weekDayLabels) {
            monthlyColumns.add(new TableColumn<Week, String>(name));

        }
        repopulateMonthlyTable(LocalDateTime.of(ld, LocalTime.MIN));
        for (int i = 0; i < monthlyColumns.size(); i++) {
            monthlyColumns.get(i).setPrefWidth(150);
            final int counter = i;
            monthlyColumns.get(i).setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Week, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<Week, String> p) {
                    if (p.getValue() != null) {
                        return new SimpleStringProperty(p.getValue().getLocalDateForCurrentDay(counter)
                                .format(formatter) + "\n" + p.getValue().getAppointment(counter));
                    } else {
                        return new SimpleStringProperty("");
                    }
                }
            });
            renderCell(monthlyColumns.get(i));
        }
        monthlyCells.addListener(this::showSingleCellDetails);
        monthlyView.getColumns().addAll(this.monthlyColumns);
        monthlyView.setPrefWidth(800);
        monthlyView.setFixedCellSize(200);
        monthlyView.getSelectionModel().setCellSelectionEnabled(true);
        return monthlyView;
    }
    // end of createCalendar methods 

    /*
    repopulateTable methods are what populuate/build the calendar views
    repopulateMonthlyTable receives the current time to work with.
    it calculates the start of each month 
    retrieves all of the appointments for that particular day with the use of the jamDAO
    builds 7 daily bean objects and gives each daily bean its own list of appointment records
    this process is done 5 times for each week in the month. I did not implement 6 weeks as the 
    standard.  After each week is built the items are set to the monthlyView
     */
    public void repopulateMonthlyTable(LocalDateTime localDateTime) {
        monthlyCells = monthlyView.getSelectionModel().getSelectedCells();
        log.debug("repopulateTable print paramater localdatetime+ " + localDateTime);
        ZoneId z = ZoneId.of("America/Montreal");
        YearMonth ym = YearMonth.from(localDateTime);

        log.debug("repopulateTable print paramater month from " + ym);
        LocalDate startOfMonth = ym.atDay(1);
        log.debug("repopulateTable start of month " + startOfMonth);
        int position = getDayOfWeekPosition(startOfMonth.getDayOfWeek().toString());
        log.debug("repopulateTable getDayOfWeekPosition from 1st of month  " + position);
        localDateTime = LocalDateTime.of(startOfMonth, LocalTime.MIN).minusDays(position);
//        log.debug("repopulateTable monthlyTimes" + monthlyTimes.size());
        ObservableList<Week> times = FXCollections.observableArrayList();
        List<SimpleObjectProperty<Day>> list = new ArrayList<SimpleObjectProperty<Day>>();
        log.debug("repopulateTable localDate time - days from position" + localDateTime);
        List<AppointmentRecord> appointments = new ArrayList<>();
        // build week
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 7; j++) {
                SimpleObjectProperty<Day> day = new SimpleObjectProperty<Day>();
                Timestamp startOfDay = Timestamp.valueOf(localDateTime.toLocalDate().atStartOfDay());
                Timestamp endOfDay = Timestamp.valueOf(localDateTime.toLocalDate().atTime(23, 59, 59));
                try {
                    appointments = this.app.getJamDAO().findAllAppointmentsBetween(startOfDay, endOfDay);
                    log.debug("createMonthlyView appointments size()" + appointments.size());
                } catch (SQLException ex) {
                    java.util.logging.Logger.getLogger(CalendarViewController.class.getName()).log(Level.SEVERE, null, ex);
                }
                day.setValue(new Day(localDateTime, appointments));
                localDateTime = localDateTime.plusDays(1);
                list.add(day);
                log.debug("createMonthlyView inside forloop get size of list of days " + list.size());
                log.debug("createMonthlyView inside forloop get date of list.get(j) " + list.get(j).getValue().getLocalDate().toString());
            }
            Week w = new Week(list, localDateTime);
            times.add(w);
            list = new ArrayList<SimpleObjectProperty<Day>>();
        }

        this.monthlyView.setItems(times);

    }

    /*
    repopulateWeekly derives from daily but the process is repeated 7 times for each day
    the only exclusive thing for repopulateWeeklyTable from repopulateDaily is 
    there are technically 48 weekly beans that are built for each 30 mins 
    in the time column.  Therefore even though each week has 7 daily ojects 
    with all their appointments. the only ones displayed are those who match 
    the date in the weekly object which is assigned every 30 mins.
    */
    public void repopulateWeeklyTable(LocalDateTime localDateTime) {
        monthlyCells = weeklyView.getSelectionModel().getSelectedCells();
        ObservableList<Week> times = FXCollections.observableArrayList();
        localDateTime = localDateTime.toLocalDate().atStartOfDay();
        List<SimpleObjectProperty<Day>> list = new ArrayList<SimpleObjectProperty<Day>>();
        int position = getDayOfWeekPosition(localDateTime.getDayOfWeek().toString());
        log.debug("createWeeklyView getDayOfWeekPosition from 1st of month  " + position);
        localDateTime = localDateTime.minusDays(position);
        log.debug("createWeeklyView get date behind to start the calendar" + localDateTime.toLocalDate());
        List<AppointmentRecord> appointments = new ArrayList<>();

        //create daily beans for week bean
        for (int i = 0; i < 7; i++) {
            weeklyColumns.add(new TableColumn<>(weekDayLabels[i] + " " + localDateTime.format(formatter)));
            Timestamp startOfDay = Timestamp.valueOf(localDateTime.toLocalDate().atStartOfDay());
            Timestamp endOfDay = Timestamp.valueOf(localDateTime.toLocalDate().atTime(23, 59, 59));
            try {
                //get all appts for that particular day
                appointments = this.app.getJamDAO().findAllAppointmentsBetween(startOfDay, endOfDay);
            } catch (SQLException ex) {
                java.util.logging.Logger.getLogger(CalendarViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
            log.debug("createWeeklyView appointments for week size for current day " + appointments.size());
            // create day bean with those appointsments and local time of 30 min block
            SimpleObjectProperty<Day> day = new SimpleObjectProperty<Day>(new Day(localDateTime, appointments));
            list.add(day);
            log.debug("inside repopulate weekly for weekdaylabels on columns" + weekDayLabels[i] + " " + localDateTime.toLocalDate().toString());
            weeklyColumns.get(i).setText(weekDayLabels[i] + " " + localDateTime.toLocalDate().toString());
            localDateTime = localDateTime.plusDays(1);
        }

        //end of 7 days of bean build
        // create 48 weekly beans for 30 min time blocks
        for (int i = 0; i < 48; i++) {
            List<SimpleObjectProperty<Day>> list2 = new ArrayList<SimpleObjectProperty<Day>>();
            for (int j = 0; j < 7; j++) {

                SimpleObjectProperty<Day> day = new SimpleObjectProperty<Day>(new Day(list.get(j).get()));
                list2.add(day);
                list2.get(j).get().setTime(localDateTime);

            }

            Week w = new Week(new ArrayList(list2), localDateTime);
            times.add(w);
            weeklyTimeCol.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getLocalDateTime().toLocalTime().toString()));
            localDateTime = localDateTime.plusMinutes(30);

        }
        weeklyView.setItems(times);
        Collections.rotate(times, -17);
    }

    /*
    repopulateDaily builds 48 daily objects and displays the appointments 
    respective to its time object 
    */
    public void repopulateDailyTable(LocalDateTime localDateTime) {
        //monthlyCells = monthlyView.getSelectionModel().getSelectedCells();
        //log.debug("Size of theCells " + monthlyCells.size());
        // Listen for selection changes for a single cell amongst the selected cells

        ObservableList<Day> times = FXCollections.observableArrayList();
        ObservableList<Day> times2 = FXCollections.observableArrayList();
        localDateTime = localDateTime.toLocalDate().atStartOfDay();
        List<AppointmentRecord> appointments = new ArrayList<>();
        Timestamp startOfDay = Timestamp.valueOf(localDateTime.toLocalDate().atStartOfDay());
        Timestamp endOfDay = Timestamp.valueOf(localDateTime.toLocalDate().atTime(23, 59, 59));
        try {
            appointments = this.app.getJamDAO().findAllAppointmentsBetween(startOfDay, endOfDay);
            log.debug("createDailyView appointments size()" + appointments.size());
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(CalendarViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < 48; i++) {
            times.add(new Day(localDateTime, appointments));
            timeCol.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getLocalDateTime().toString()));
            localDateTime = localDateTime.plusMinutes(30);
        }
        dailyView.setItems(times);
        Collections.rotate(times, -17);
    }

    /*
    getDayOfWeek is used mainly in the repopulateWeekly and repopulateMonthly 
    to know how many days the calendar must subtract to start the display of sunday-saturday
    */
    private int getDayOfWeekPosition(String dayOfWeek) {
        for (int i = 0; i < weekDayLabels.length; i++) {
            if (weekDayLabels[i].equalsIgnoreCase(dayOfWeek)) {
                return i;
            }
        }

        return -1;
    }

    /*
    a small private method to style the cells
    */
    private void renderCell(TableColumn tc) {

        tc.setCellFactory(column -> {
            return new TableCell<Week, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item);
                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {

                        log.debug(tc.getCellData(0) + "get cell observable value");
                        setTextFill(Color.BLACK);
                        setStyle(".table-row-cell{\n"
                                + "-fx-border-color: #ef3f48;\n"
                                + "-fx-table-cell-border-color:#ef3f48;}");
                    }
                }
            };
        });

    }

    public void setRoot(MainApp app) {
        this.app = app;

    }

    /*
    this method is used as the callback of the cell click event.  Either for the monthly or the weekly view 
    it then invokes the dialogSingleCellDetails() method to create the alert boxes on what the user can 
    do after a populated cell click has occured
    */
    private void showSingleCellDetails(ListChangeListener.Change<? extends TablePosition> change) {
        if (monthlyCells.size() > 0) {
            TablePosition selectedCell = monthlyCells.get(0);
            TableColumn column = selectedCell.getTableColumn();
            int rowIndex = selectedCell.getRow();
            Object data = column.getCellObservableValue(rowIndex).getValue();

            log.info("value = " + data.toString().length());
            if (data.toString().length() < 4) {
                return;
            }
            dialogSingleCellDetails((String) data);
            log.info("value = " + (String) data);
        }
    }

    /**
     * alert dialog method to ask the user what they want todo when they click on a populated cell
     * choices are either go to that particular day and switch the view to the daily or go to the form 
     * with the first record from the cell.
     * @param data
     */
    private void dialogSingleCellDetails(String data) {

        log.debug(data.toString());
        int index = data.toString().indexOf("20");
        int indexOfColon = data.toString().indexOf(":");
        String s = data.substring(index, indexOfColon - 3);
        alert.setHeaderText(data.toString());
        alert.setContentText("Choose your option.");
        alert.setTitle("Day Details" + s);
        ButtonType buttonTypeOne = new ButtonType("Go to day");
        ButtonType buttonTypeTwo = new ButtonType("View appointment in form");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeCancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            LocalDateTime dateClicked = LocalDateTime.of(LocalDate.parse(s), LocalTime.MIN);
            this.app.getCalendarController().repopulateDailyTable(dateClicked);
            this.app.getMainLayoutController().onViewChange("dailyView");
        } else if (result.get() == buttonTypeTwo) {
            LocalDateTime dateClickedStart = LocalDateTime.of(LocalDate.parse(s), LocalTime.MIN);
            log.debug("on form click with dateClickedStart" + dateClickedStart);
            LocalDateTime dateClickedEnd = LocalDateTime.of(LocalDate.parse(s), LocalTime.MAX);
            log.debug("on form click with dateClickedEnd" + dateClickedEnd);
            Timestamp t1 = Timestamp.valueOf(dateClickedStart);
            Timestamp t2 = Timestamp.valueOf(dateClickedEnd);
            try {
                List<AppointmentRecord> list = this.app.getJamDAO().findAllAppointmentsBetween(t1, t2);
                log.debug(list.get(0).toString() + "found record findallAppointmentsBetween");
                this.app.getAppointmentController().setClickedAppointmentRecord(list.get(0));
                this.app.getMainLayoutController().onViewChange("createNewAppointmentBtn");
            } catch (SQLException ex) {
                java.util.logging.Logger.getLogger(CalendarViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
