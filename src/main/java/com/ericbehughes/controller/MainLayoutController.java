package com.ericbehughes.controller;

import com.ericbehughes.database.JamDAO;
import com.ericbehughes.jam_1412844_1.MainApp;
import com.ericbehughes.models.AppointmentGroupRecord;
import com.ericbehughes.models.AppointmentRecord;
import com.ericbehughes.models.Smtp;
import com.ericbehughes.models.Week;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author ehugh
 */
public class MainLayoutController implements Initializable {
    // Real programmers use logging

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());
    private JamDAO jamDAO;

    private ObservableList<Node> views;

    /**
     * menu buttons for the toolbar to switch between forms and calendar
     */
    @FXML
    private MenuItem createNewAppointmentBtn;

    @FXML
    private MenuItem createNewAppointmentGroupBtn;

    @FXML
    private MenuItem createNewSmtpBtn;

    @FXML
    private MenuItem configDatabaseCredentialsBtn;

    /*
    borderView is the main borderview of the entire application
     */
    private BorderPane borderView;

    /*
    i made a separate fxml for the bottom bar when the calendar view is selected
     */
    private AnchorPane bottomBar;


    /*
    currentDateTV is the date displayed in between the previous and next buttons on the calendar view
     */
    @FXML
    private Text currentDateTV;

    /*
    the stackpane is the container that all the views are stacked and is set in the center of this.borderView
     */
    private StackPane sp;

    /*
    Mainapp reference variable to pass to child views when neeeded
     */
    private MainApp app;

    /*
    the current date that is used when the application runs and what is used for the previous and next buttons
     */
    private LocalDate ld = LocalDate.now();
    /*
    currentView is a string that holds the currentView selected every time the view changes to help the stack pane behavior
     */
    private String currentView;

    public MainLayoutController() {
        super();

    }

    /**
     * Initializes the controller class. by default the monthly calendar view is
     * chosen
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        currentView = "monthlyView";
    }

    /*
    a click listener for the toolbar in the borderView view.  It is wired to all the menu items in the toolbar
    and invokes onViewChange() and swapView() to check which view is going to be displayed to the user
     */
    public void onMenuClick(ActionEvent e) {
        //get all the views of the stackpane
        this.views = this.sp.getChildren();
        if (views.size() > 1) {
            MenuItem n = (MenuItem) e.getSource();
            String id = n.getId();
            log.debug(" onCreateMenuClick" + id);
            onViewChange(id);

        }

    }

    /*
    onViewChange() receives a string from the onMenuClick and swaps the view of the stackpane
    depending on the view it also sets the bottom of the this.borderView to null unless the 
    calendar view is selected
     */
    public void onViewChange(String id) {
        if (id.equals("createNewAppointmentBtn")) {
            swapView("appointmentView");
            log.debug(this.borderView.getChildren().toString());
            this.borderView.setBottom(null);
        } else if (id.equals("createNewAppointmentGroupBtn")) {
            swapView("appointmentGroupView");
            log.debug(this.borderView.getChildren().toString());
            this.borderView.setBottom(null);
        } else if (id.equals("createNewSmtpBtn")) {
            swapView("smtpView");
            log.debug(this.borderView.getChildren().toString());
            this.borderView.setBottom(null);
        } else if (id.equals("showCalendarBtn")) {
            swapView("monthlyView");
            this.borderView.setBottom(this.bottomBar);
            log.debug(this.borderView.getChildren().toString());
        } else if (id.equals("dailyView")) {
            swapView("dailyView");
            this.borderView.setBottom(this.bottomBar);
            log.debug(this.borderView.getChildren().toString());
        } else if (id.equals("configDatabaseCredentialsBtn")) {
            swapView("databaseConfigView");
            this.borderView.setBottom(null);
            log.debug(this.borderView.getChildren().toString());

        }
    }
    
    /*
    swapView() receives the id of the view desired to be popped to the front of the stackpane
    it also assigns the currentView field to the swapped view
    */
    private void swapView(String id) {
        log.debug("this.borderView" + this.borderView);
        log.debug("swap view this.sp.getChildren" + this.sp);
        this.views = this.sp.getChildren();
        log.debug(this.views + "this.views");
        log.debug("view size" + this.views.size());
        for (int i = 0; i < views.size(); i++) {
            if (views.get(i).getId().equals(id)) {
                Node topNode = views.get(i);
                log.debug("swap view and id is " + views.get(i).getId());
                currentView = views.get(i).getId();
                topNode.toFront();

                log.debug("currentView after swap view" + currentView);
                break;

            }
        }

    }

    /*
    in the calendar view, the user has buttons in the bottom toolbar to switch between
    daily, weekly or monthly views.  this listener receives the id of any of the 3 buttons
    and uses the swapView method to switch to the desired view.  It also assigns the date
    to currentDateTV 
    */
    public void switchCalendarViewType(ActionEvent e) {
        log.debug(this.sp.toString());
        log.debug("inside switchCalendarViewType");
        Button b = (Button) e.getSource();
        String id = b.getId();
        log.debug(id);

        if (id.equals("viewDayBtn")) {
            swapView("dailyView");
            currentView = "dailyView";
            currentDateTV.setText(this.app.getCurrentTime().toLocalDate().toString());
        } else if (id.equals("viewWeekBtn")) {
            swapView("weeklyView");
            currentView = "weeklyView";
            currentDateTV.setText("");
        } else if (id.equals("viewMonthBtn")) {
            swapView("monthlyView");
            currentView = "monthlyView";
            currentDateTV.setText(this.app.getCurrentTime().toLocalDate().toString());
        }
        log.debug("currentView " + currentView);

    }

    /*
    in the calendar view, the user has buttons to navigate between month, week or day
    previous and next arrow changes the date of the current calendar view type
    however because the previous and next buttons in the root layout and 
    not in the stackpane the listeners are actually in the main app
    */
    public void onPreviousArrowClick(ActionEvent e) {

        this.app.onPreviousArrowClick(this.app.getCurrentTime().toLocalDate(), currentView);
        if (!currentView.equals("weeklyView")) {
            currentDateTV.setText(this.app.getCurrentTime().toLocalDate().toString());
        }
    }
    public void onNextArrowClick(ActionEvent e) {
        log.debug("inside next arrow click");

        this.app.onNextArrowClick(this.app.getCurrentTime().toLocalDate(), currentView);
        if (!currentView.equals("weeklyView")) {
            currentDateTV.setText(this.app.getCurrentTime().toLocalDate().toString());
        }
    }

    /*
    set root sets the stackpane and sets the main app reference for this controller
    */
    public void setRoot(BorderPane n) {
        log.debug("inside setRoot MainlayoutController");
        this.borderView = (BorderPane) n;
        this.bottomBar = (AnchorPane) n.getBottom();
        log.debug(this.borderView + " inside setRoot borderVIew");
        //container.setPrefHeight(50);
        //this.borderView.setBottom(container);
        log.debug(this.borderView.getChildren().toString());
        for (Node view : this.borderView.getChildren()) {
            if (view instanceof StackPane) {
                this.sp = (StackPane) view;
                this.views = this.sp.getChildren();
                log.debug(this.views.size() + "this.views stackpane children");
                log.debug(this.views.toString());
                break;
            }

        }
        log.debug("end of setRoot" + this.views.toString());
    }

    /*
    set setMainApp sets  the main app reference for this controller
    can be combined with setRoot but seperated the logic
    */
    public void setMainApp(MainApp app) {
        this.app = app;
        currentDateTV.setText(this.ld.toString());
    }

}
