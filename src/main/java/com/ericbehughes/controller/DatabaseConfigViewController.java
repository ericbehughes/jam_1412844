package com.ericbehughes.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.ericbehughes.database.JamDAO;
import com.ericbehughes.jam_1412844_1.MainApp;
import com.ericbehughes.models.DatabaseCredentials;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author ehugh
 */
public class DatabaseConfigViewController implements Initializable {

    private JamDAO jamDAO;
    private DatabaseCredentials credentials;
    private MainApp app;
    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass().getName());
    @FXML
    private TextField tfDatabaseUsername;
    @FXML
    private TextField tfDatabasePassword;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    private void doBindings() {
        log.debug("inside do bindings for databasecongif controller" + credentials.toString());

        Bindings.bindBidirectional(tfDatabaseUsername.textProperty(), credentials.getUserProperty());
        Bindings.bindBidirectional(tfDatabasePassword.textProperty(), credentials.getPasswordProperty());

    }

    @FXML
    private void onDatabaseConfigSave(Event e) {
        log.debug("com.ericbehughes.controller.DatabaseConfigViewController.onDatabaseConfigSave" + this.credentials.toString());
        this.app.getJamDBUtility().changeCredentials(this.credentials);

    }

    @FXML
    private void onDatabaseConfigClear(Event e) {
        this.credentials.setUser("");
        this.credentials.setPassword("");

    }

    @FXML
    private void onDatabaseConfigExit(Event e) {
        log.debug("com.ericbehughes.controller.DatabaseConfigViewController.onDatabaseConfigExit" + this.credentials.toString());
        

    }

    public void setRoot(MainApp app) {
        this.app = app;
        this.credentials = app.getJamDBUtility().getCredentials();
        doBindings();
    }

}
