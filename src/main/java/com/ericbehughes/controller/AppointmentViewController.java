/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ericbehughes.controller;

import com.ericbehughes.database.JamDAO;
import com.ericbehughes.models.AppointmentGroupRecord;
import com.ericbehughes.models.AppointmentRecord;
import com.ericbehughes.models.Week;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Menu;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author ehugh
 */
public class AppointmentViewController implements Initializable {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH:mm");
    /**
     * Initializes the controller class.
     */
    // model
    private AppointmentRecord appointmentRecordModel;

    // JAM DAO object
    private JamDAO jamDAO;
    @FXML
    private AnchorPane appointmentView;
    @FXML
    private Button appointmentCancelBtn;

    @FXML
    private Button appointmentSaveBtn;

    @FXML
    private Button appointmentNextBtn;

    @FXML
    private Button appointmentPreviousBtn;

    @FXML
    private TextField appointmentViewId;

    @FXML
    private TextField appointmentViewTitle;

    @FXML
    private TextArea appointmentViewDetails;

    @FXML
    private TextField appointmentViewLocation;
    //appointmentViewLocation

    @FXML
    private ComboBox<String> appointmentViewGroupBox;

    @FXML
    private CheckBox requestAlarmViewId;

    @FXML
    private CheckBox wholeDayViewId;

    @FXML
    private DatePicker startTimeDatePicker;

    @FXML
    private ComboBox<String> startTimeComboBox;

    @FXML
    private DatePicker endTimeDatePicker;

    @FXML
    private ComboBox<String> endTimeComboBox;

    private Alert alert = new Alert(AlertType.CONFIRMATION);

    public AppointmentViewController() {
        super();
    }
    private List<AppointmentRecord> appointmentsByTitle = new ArrayList<>();
    private int currentAppointmentIndex = 0;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void onAppointmentPrevious(Event e) {
        log.debug("com.ericbehughes.controller.AppointmentGroupViewController.onAppointmentPrevious");
        if (appointmentsByTitle.size() > 0) {
            setClickedAppointmentRecord(appointmentsByTitle.get(--currentAppointmentIndex));
        } else {
            try {
                jamDAO.findPrevByID(this.appointmentRecordModel);
                log.debug("end of findNextByID " + appointmentRecordModel.toString());
            } catch (Exception ex) {
                log.error("SQL Error", ex);
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @FXML
    private void onAppointmentNext(Event e) {
        log.debug("com.ericbehughes.controller.AppointmentGroupViewController.onAppointmentNext");
        if (appointmentsByTitle.size() > 0) {
            setClickedAppointmentRecord(appointmentsByTitle.get(++currentAppointmentIndex));
        } else {
            try {
                jamDAO.findNextByID(this.appointmentRecordModel);
                log.debug("end of findNextByID " + appointmentRecordModel.toString());
            } catch (Exception ex) {
                log.error("SQL Error", ex);
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /*
    click listener for save button
    validates if the date chosen for the appointment respects normal appointment conventions with an alert dialog
    checks if the appointment is not in the database, if not the appointment is created
    otherwise a simple update to the db happens
    */
    @FXML
    private void onAppointmentSave(Event e) {
        log.debug("com.ericbehughes.controller.AppointmentGroupViewController.onAppointmentSave");

        log.debug("startTime " + this.appointmentRecordModel.getStartTime().toLocalDateTime());
        log.debug("endTime " + this.appointmentRecordModel.getEndTime().toLocalDateTime());
        // start must be before end and same day
        if (this.appointmentRecordModel.getStartTime().toLocalDateTime().isBefore(this.appointmentRecordModel.getEndTime().toLocalDateTime())
                && this.appointmentRecordModel.getStartTime().toLocalDateTime().getDayOfWeek() == this.appointmentRecordModel.getEndTime().toLocalDateTime().getDayOfWeek()) {
            try {
                AppointmentRecord temp = jamDAO.findAppointmentByID(this.appointmentRecordModel.getID());
                if (temp.getID() == -1) {
                    log.debug("start onAppointmentSave create" + this.appointmentRecordModel);
                    jamDAO.create(this.appointmentRecordModel);
                    log.debug("end of onAppointmentSave create " + appointmentRecordModel.toString());
                } else {
                    log.debug("start onAppointmentSave update" + this.appointmentRecordModel);
                    jamDAO.update(this.appointmentRecordModel);
                    log.debug("end onAppointmentSave update" + appointmentRecordModel.toString());
                }

            } catch (Exception ex) {
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Warning");
            alert.setHeaderText("Time Constraint");
            alert.setContentText("Start time must be before End time, thanks.");

            alert.showAndWait();

        }

    }

    /*
    find click listener for the find button
    will find all appointments with the given title in the title textfield
    then proviedes that list to the next and previous button handlers
    
    if the textfield is blank then the search begins back at the first record found
    */
    @FXML
    public void onAppointmentFind() {
        log.debug("com.ericbehughes.controller.AppointmentGroupViewController.onAppointmentFind");
        try {
            log.debug("appointmentModel" + this.appointmentRecordModel);
            appointmentsByTitle = jamDAO.findAllAppointments();
            if (this.appointmentRecordModel.getTitle().length() == 0) {
                appointmentsByTitle.clear();
                onAppointmentNext(null);
            }

            appointmentsByTitle.removeIf(a -> !a.getTitle().equalsIgnoreCase(appointmentRecordModel.getTitle()));
            log.debug("appointsments found with title " + this.appointmentRecordModel.getTitle());
            for (AppointmentRecord a : appointmentsByTitle) {
                log.debug(a.toString());
            }
            if (appointmentsByTitle.size() > 0) {
                this.setClickedAppointmentRecord(appointmentsByTitle.get(0));
            }
            log.debug("end of appointmentSave " + appointmentRecordModel.toString());
        } catch (Exception ex) {
            Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
    click listener for the clear button 
    sets the current bound model to default constructor
    */
    @FXML
    public void onAppointmentClear() {
        log.debug("com.ericbehughes.controller.AppointmentGroupViewController.onAppointmentClear");
        this.setClickedAppointmentRecord(new AppointmentRecord());
        this.appointmentsByTitle.clear();
    }

    /*
    click listener for the delete button 
    a warning dialog is shown to the user to confirm the delete action
    if user clicks OK the delete is performed otherwise close the dialog
    */
    @FXML
    public void onAppointmentDelete() {
        log.debug("com.ericbehughes.controller.AppointmentGroupViewController.onAppointmentDelete");

        alert.setTitle("Warning");
        alert.setHeaderText("Deleting Appointment");
        alert.setContentText("Once deleted you cannot retrieve this appointment");

        alert.showAndWait();

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            try {
                jamDAO.deleteAppointment(this.appointmentRecordModel.getID());
                log.debug("end of findNextByID " + appointmentRecordModel.toString());
                onAppointmentNext(null);
            } catch (Exception ex) {
                log.error("SQL Error", ex);
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            // ... user chose CANCEL or closed the dialog
        }

    }

    /*
    do bindings binds the model from the main app reference
    every javafx control in the apppointment form is bound to the models properties
    small logic is performed to populate the combo box for time and appointment group box
    */
    private void doBindings() {
        DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH:mm");
        log.debug("do bindings appointment tostring " + appointmentRecordModel.toString());
        Bindings.bindBidirectional(appointmentViewId.textProperty(), appointmentRecordModel.getIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(appointmentViewTitle.textProperty(), appointmentRecordModel.getTitleProperty());
        Bindings.bindBidirectional(appointmentViewLocation.textProperty(), appointmentRecordModel.getLocationProperty());
        log.debug("starDateProperty" + Timestamp.valueOf(LocalDateTime.now()).toLocalDateTime().toLocalDate());
        Bindings.bindBidirectional(startTimeDatePicker.valueProperty(), appointmentRecordModel.getStartDateProperty());

        ObservableList<String> timeList = FXCollections.observableArrayList();
        LocalDateTime ldt = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);
        for (int i = 0; i < 288; i++) {
            timeList.add(ldt.format(hourFormatter));
            ldt = ldt.plusMinutes(5);
        }
        startTimeComboBox.setItems(timeList);
        endTimeComboBox.setItems(timeList);
        Bindings.bindBidirectional(startTimeComboBox.valueProperty(), appointmentRecordModel.getStartTimeProperty());

        Bindings.bindBidirectional(endTimeDatePicker.valueProperty(), appointmentRecordModel.getEndDateProperty());
        Bindings.bindBidirectional(endTimeComboBox.valueProperty(), appointmentRecordModel.getEndTimeProperty());
        ObservableList<String> groupNums = FXCollections.observableArrayList();
        try {
            List<AppointmentGroupRecord> list = jamDAO.findAllGroups();
            for (AppointmentGroupRecord appointmentGroupRecord : list) {
                groupNums.add((String.valueOf(appointmentGroupRecord.getID())));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AppointmentViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
        appointmentViewGroupBox.setItems(groupNums);
        Bindings.bindBidirectional(appointmentViewGroupBox.valueProperty(), appointmentRecordModel.getAppointmentGroupProperty(), new NumberStringConverter());

        Bindings.bindBidirectional(appointmentViewDetails.textProperty(), appointmentRecordModel.getDetailsProperty());
        //requestAlarmViewId
        Bindings.bindBidirectional(requestAlarmViewId.selectedProperty(), appointmentRecordModel.getAlarmReminderRequestedBooleanProperty());
        Bindings.bindBidirectional(wholeDayViewId.selectedProperty(), appointmentRecordModel.getWholeDayBooleanPropertyProperty());

        

    }
   
    /*
    set the jamDAO object from the main app to perform DAO actions
    */
    public void setJamDAO(AppointmentRecord a, JamDAO jamDAO) {
        log.debug("Appointment view controller start setJamDAO " + jamDAO);

        this.appointmentRecordModel = a;

        log.debug("Appointment view controller start setJamDAO " + this.appointmentRecordModel);
        try {
            log.debug("appointviewcontroller inside setJamDAO jamDAO print" + jamDAO);
            this.jamDAO = jamDAO;
        } catch (Exception ex) {
            log.error("SQL Error", ex);
        }

        try {
            jamDAO.findNextByID(a);
            log.debug("inside find nextByID  of appointmentRecord " + appointmentRecordModel.toString());
        } catch (SQLException ex) {
            Logger.getLogger(AppointmentGroupViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
        doBindings();
        log.debug("appointmentviewcontroller after do bindings " + appointmentRecordModel.getStartTime());
        LocalDateTime l = LocalDateTime.of(a.getStartDateProperty().get(), LocalTime.parse(a.getStartTimeProperty().get()));
        Timestamp t = Timestamp.valueOf(l);

    }

    /*
    sets the bound model from the main app to an appointment records fields
    used in clear method and find method
    */
    public void setClickedAppointmentRecord(AppointmentRecord a) {
        log.debug("setCLickedAppointmentRecord" + a.getID());

        this.appointmentRecordModel.setID(a.getID());
        this.appointmentRecordModel.setTitle(a.getTitle());
        this.appointmentRecordModel.setLocation(a.getLocation());
        this.appointmentRecordModel.setStartTimeStamp(a.getStartTime());
        this.appointmentRecordModel.setEndTimeStamp(a.getEndTime());
        this.appointmentRecordModel.setDetails(a.getDetails());
        this.appointmentRecordModel.setAppointmentGroup(a.getAppointmentGroup());
        this.appointmentRecordModel.setAlarmReminderRequested(a.getAlarmReminderRequested());
        this.appointmentRecordModel.setWholeDay(a.getWholeDay());

    }

}
