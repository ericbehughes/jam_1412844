package com.ericbehughes.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.ericbehughes.database.JamDAO;
import com.ericbehughes.models.AppointmentGroupRecord;
import com.ericbehughes.models.AppointmentRecord;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Menu;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author ehugh
 */
public class AppointmentGroupViewController implements Initializable {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass().getName());

    // model
    private AppointmentGroupRecord appointmentGroupModel;

    // JAM DAO object
    private JamDAO jamDAO;
    @FXML
    private AnchorPane appointmentGroupView;
    @FXML
    private Button appointmentGroupDoneBtn;

    @FXML
    private Button appointmentGroupSaveBtn;

    @FXML
    private Button appointmentGroupNextBtn;

    @FXML
    private Button appointmentGroupPreviousBtn;

    @FXML
    private TextField apptGroupID;
    @FXML
    private TextField apptGroupName;

    @FXML
    private ColorPicker colorPicker;
    private Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //log.debug("inside do initialize appointviewgroup controller for appointgroup" + appointmentGroupModel.toString());
        //doBindings();
    }

    private void doBindings() {
        log.debug("inside do bindings for appointgroup" + appointmentGroupModel.toString());
        log.debug("inside do bindings for appointgroup group name" + appointmentGroupModel.getGroupName());
        Bindings.bindBidirectional(apptGroupID.textProperty(), appointmentGroupModel.getIDProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(apptGroupName.textProperty(), appointmentGroupModel.getGroupNameProperty());
        log.debug(colorPicker.valueProperty() + "get colorPikcerValueProperty");
        colorPicker.setOnAction(new EventHandler() {
            public void handle(Event t) {
                log.debug("inside handle of color picker " + colorPicker.getValue());
            }
        });
        Bindings.bindBidirectional(colorPicker.valueProperty(), appointmentGroupModel.getColorProperty());
    }

    @FXML
    private void onAppointmentGroupPrevious(Event e) {
        log.debug("com.ericbehughes.controller.AppointmentGroupViewController.onAppointmentGroupPrevious");
            try {
                jamDAO.findPrevByID(this.appointmentGroupModel);
                log.debug("end of findNextByID " + appointmentGroupModel.toString());
            } catch (Exception ex) {
                log.error("SQL Error", ex);
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            
        }

    }

    @FXML
    private void onAppointmentGroupNext(Event e) {
        log.debug("com.ericbehughes.controller.AppointmentGroupViewController.onAppointmentGroupNext");
            try {
                jamDAO.findNextByID(this.appointmentGroupModel);
                log.debug("end of findNextByID " + appointmentGroupModel.toString());
            } catch (Exception ex) {
                log.error("SQL Error", ex);
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        

    }

    @FXML
    private void onAppointmentGroupSave(Event e) {
        log.debug("com.ericbehughes.controller.AppointmentGroupViewController.onAppointmentGroupSave");

        
            try {
                AppointmentGroupRecord temp = jamDAO.findAppointmentGroupRecordById(this.appointmentGroupModel.getID());
                if (temp.getID() == -1) {
                    log.debug("start onAppointmentGroupSave create" + this.appointmentGroupModel);
                    jamDAO.create(this.appointmentGroupModel);
                    log.debug("end of onAppointmentGroupSave create " + appointmentGroupModel.toString());
                } else {
                    log.debug("start onAppointmentGroupSave update" + this.appointmentGroupModel);
                    jamDAO.update(this.appointmentGroupModel);
                    log.debug("end onAppointmentGroupSave update" + appointmentGroupModel.toString());
                }

            } catch (Exception ex) {
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        

    }

    @FXML
    public void onAppointmentGroupClear() {
        log.debug("com.ericbehughes.controller.AppointmentGroupViewController.onAppointmentGroupClear");
        this.setClickedAppointmentGroupRecord(new AppointmentGroupRecord());
    }

    @FXML
    public void onAppointmentGroupDelete() {
        log.debug("com.ericbehughes.controller.AppointmentGroupViewController.onAppointmentGroupDelete");
        alert.setTitle("Warning");
        alert.setHeaderText("Deleting AppointmentGroup");
        alert.setContentText("Once deleted you cannot retrieve this appointment group");

        alert.showAndWait();

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            try {
                jamDAO.deleteAppointmentGroup(this.appointmentGroupModel.getID());
                log.debug("end of deleteAppointmentGroup " + appointmentGroupModel.toString());
                onAppointmentGroupNext(null);
            } catch (Exception ex) {
                log.error("SQL Error", ex);
                Logger.getLogger(MainLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            // ... user chose CANCEL or closed the dialog
        }

    }
    public void setJamDAO(AppointmentGroupRecord agr, JamDAO jamDAO) {
        this.appointmentGroupModel = agr;
        this.jamDAO = jamDAO;

        try {
            log.debug(" printing agr getID" + agr.getID());
            this.jamDAO.findNextByID(agr);
            log.debug("inside setJamDAO" + appointmentGroupModel.getGroupName());
        } catch (SQLException ex) {
            Logger.getLogger(AppointmentGroupViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
        doBindings();

    }

    private void setClickedAppointmentGroupRecord(AppointmentGroupRecord agr) {
        this.appointmentGroupModel.setID(agr.getID());
        this.appointmentGroupModel.setGroupName(agr.getGroupName());
        this.appointmentGroupModel.setColor(agr.getColor());
    }
    

}
