/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ericbehughes.jam_1412844_1;

import com.ericbehughes.controller.AppointmentGroupViewController;
import com.ericbehughes.controller.AppointmentViewController;
import com.ericbehughes.controller.DatabaseConfigViewController;
import com.ericbehughes.controller.CalendarViewController;
import com.ericbehughes.controller.MainLayoutController;
import com.ericbehughes.controller.SmtpViewController;
import com.ericbehughes.database.JamDAO;
import com.ericbehughes.database.JamDbUtil;

import com.ericbehughes.models.AppointmentGroupRecord;
import com.ericbehughes.models.AppointmentRecord;
import com.ericbehughes.models.Day;
import com.ericbehughes.models.Smtp;
import com.ericbehughes.models.Week;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ehugh
 */
public class MainApp extends Application {

    // rootView
    @FXML
    private BorderPane borderView;
    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());
    //models for all views
    private AppointmentRecord a;
    private AppointmentGroupRecord agr;
    private Smtp smtp;
    private final JamDAO jamDAO;
    private AppointmentRecord appRecordData;
    private StackPane sp;
    private CalendarViewController calendarController;

    private MainLayoutController mainLayoutController;

    private LocalDateTime currentTime = LocalDateTime.now();
    private JamDbUtil jamDBUtility;

    @FXML
    TableView<Day> dailyView = new TableView<>();

    @FXML
    TableView<Week> weeklyView = new TableView<>();

    @FXML
    public TableView<Week> monthlyView = new TableView<>();
    private AppointmentViewController appointmentController;
    
    

    public MainApp() {
        super();
        log.debug("inside main app constructor");
        this.sp = new StackPane();
        jamDBUtility = new JamDbUtil();
        log.debug("start getJAMDAO");
        this.jamDAO = jamDBUtility.getJamDAO();
        this.a = new AppointmentRecord();
        this.agr = new AppointmentGroupRecord();
        this.smtp = new Smtp();

    }

    public JamDAO getJamDAO() {
        return jamDAO;
    }

    public JamDbUtil getJamDBUtility() {
        return jamDBUtility;
    }

    public LocalDateTime getCurrentTime() {
        return currentTime;
    }

    @Override
    public void start(Stage stage) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainLayout.fxml"));
        BorderPane root = (BorderPane) loader.load();
        this.borderView = root;
        // initalize all views 
        initRootLayout();
        this.borderView.setCenter(this.sp);

        mainLayoutController = loader.getController();
        mainLayoutController.setRoot(root);
        mainLayoutController.setMainApp(this);

        Scene scene = new Scene(root);
        stage.setTitle("JAM Calendar");
        stage.setScene(scene);
        
        

        stage.show();
    }

    public void initRootLayout() throws IOException, SQLException {

        //load all layouts here
        FXMLLoader appointmentloader = new FXMLLoader();
        appointmentloader.setLocation(getClass().getResource("/fxml/AppointmentView.fxml"));
        AnchorPane appointmentView = (AnchorPane) appointmentloader.load();
        // set id for view to enable swapping/popping with stackpane
        appointmentView.setId("appointmentView");
        appointmentController = appointmentloader.getController();
        appointmentController.setJamDAO(this.a, jamDAO);
        sp.getChildren().add(appointmentView);

        FXMLLoader appointmentGroupLoader = new FXMLLoader();
        appointmentGroupLoader.setLocation(getClass().getResource("/fxml/AppointmentGroupView.fxml"));
        AnchorPane appointmentGroupView = (AnchorPane) appointmentGroupLoader.load();
        // set id for view to enable swapping/popping with stackpane
        appointmentGroupView.setId("appointmentGroupView");
        AppointmentGroupViewController appointmentGroupcontroller = appointmentGroupLoader.getController();
        appointmentGroupcontroller.setJamDAO(this.agr, jamDAO);
        sp.getChildren().add(appointmentGroupView);

        FXMLLoader smtpLoader = new FXMLLoader();
        smtpLoader.setLocation(getClass().getResource("/fxml/SmtpView.fxml"));
        AnchorPane smtpView = (AnchorPane) smtpLoader.load();
        // set id for view to enable swapping/popping with stackpane
        smtpView.setId("smtpView");
        SmtpViewController smtpController = smtpLoader.getController();
        log.debug(smtpController + "this is smtp controller");
        smtpController.setJamDAO(this.smtp, jamDAO);
        sp.getChildren().add(smtpView);
        log.debug("this.smtp" + this.smtp.toString());
        this.smtp = jamDAO.findDefaultSmtp();
        
        
        
        FXMLLoader databaseConfigLoader = new FXMLLoader();
        databaseConfigLoader.setLocation(getClass().getResource("/fxml/DatabaseConfigView.fxml"));
        AnchorPane databaseConfigView = (AnchorPane) databaseConfigLoader.load();
        // set id for view to enable swapping/popping with stackpane
        databaseConfigView.setId("databaseConfigView");
        DatabaseConfigViewController databaseConfigController = databaseConfigLoader.getController();
        log.debug(databaseConfigController + "this is databaseConfig controller");
        databaseConfigController.setRoot(this);
        sp.getChildren().add(databaseConfigView);

        //calenar view has a stackpane with 3 views stacked on top of one another
        // all built dynamically with 3 methods from the CalendarController
        FXMLLoader calendarLoader = new FXMLLoader();
        calendarLoader.setLocation(getClass().getResource("/fxml/CalendarView.fxml"));
        AnchorPane calendarView = (AnchorPane) calendarLoader.load();
        this.calendarController = calendarLoader.getController();
        calendarController.setRoot(this);
        
        dailyView = calendarController.createDailyView(currentTime.toLocalDate());
        log.debug(dailyView.toString() + "dailyView.toString()");
        dailyView.setId("dailyView");
        weeklyView = calendarController.createWeeklyView(currentTime.toLocalDate());
        weeklyView.setId("weeklyView");

        monthlyView = calendarController.createMonthlyView(currentTime.toLocalDate());
        monthlyView.setId("monthlyView");
        sp.getChildren().add(dailyView);
        sp.getChildren().add(weeklyView);
        sp.getChildren().add(monthlyView);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /*
        see onNextArrowClick inside of calendar view.  these buttons are located 
    in the main borderView but are used when the calendarview is in the center 
    of the borderView pane.  therefore the calendarcontroller uses its main app 
    reference to invoke these listeners and change the time frame for the calendar views
     */
    public void onNextArrowClick(LocalDate ld, String view) {
        log.debug("onNextArrowClick Main app " + ld + "currentView " + view);
        LocalDateTime ldt = LocalDateTime.of(ld, LocalTime.MIN);
        if (view.equals("dailyView")) {
            log.debug("inside onNextArrowClick daily view");
            this.currentTime = ldt.plusDays(1);
            this.calendarController.repopulateDailyTable(currentTime);
        } else if (view.equals("weeklyView")) {
            log.debug("inside onNextArrowClick weeklyview");
            this.currentTime = ldt.plusWeeks(1);
            this.calendarController.repopulateWeeklyTable(currentTime);
        } else if (view.equals("monthlyView")) {
            log.debug("inside onNextArrowClick monthly view");
            this.currentTime = ldt.plusMonths(1);
            this.calendarController.repopulateMonthlyTable(currentTime);
        }
    }

    public void onPreviousArrowClick(LocalDate ld, String view) {
        log.debug("onPreviousArrowClick Main app " + ld + "currentView " + view);
        LocalDateTime ldt = LocalDateTime.of(ld, LocalTime.MIN);
        if (view.equals("dailyView")) {
            log.debug("onPreviousArrowClick daily view");
            this.currentTime = ldt.minusDays(1);
            this.calendarController.repopulateDailyTable(currentTime);
        } else if (view.equals("weeklyView")) {
            log.debug("onPreviousArrowClick weekly view");
            this.currentTime = ldt.minusWeeks(1);
            this.calendarController.repopulateWeeklyTable(currentTime);
        } else if (view.equals("monthlyView")) {
            log.debug("onPreviousArrowClick monthly view");
            this.currentTime = ldt.minusMonths(1);
            this.calendarController.repopulateMonthlyTable(currentTime);
        }
    }

    /*
    getters for the main controller, calendar and appointment controller
    you can right click on each method and see its usages 
    the mainlayout controller has the listeners to swap the views
    getMainLayoutController is used in CalendarViewController, DatabaseConfigController and 
    MainLayoutController. the main use these getters is when you click on a single cell in the monthly 
    or weekly calendar you load the daily view. 
    
     */
    public MainLayoutController getMainLayoutController() {
        return mainLayoutController;
    }

    public CalendarViewController getCalendarController() {
        return calendarController;
    }

    public AppointmentViewController getAppointmentController() {
        return appointmentController;
    }

}
